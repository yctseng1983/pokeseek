﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Global 的摘要描述
/// </summary>
public class Global
{
	public Global()
	{
		//
		// TODO: 在這裡新增建構函式邏輯
		//
	}
    public static void record(string target)
    {
        string account_sn = Global.get_account_sn();
        if (account_sn == "") return;

        string sql = "";
        //# 在抓資料庫
        Common.MSSQL_M db = new Common.MSSQL_M();
        ArrayList par = new ArrayList();
        sql = "select * from records where target=@target and account_sn=@account_sn";
        par.Clear();
        par.Add(new SqlParameter("@account_sn", account_sn));
        par.Add(new SqlParameter("@target", target));
        DataTable dt = db.Select(sql, par);

        if (dt.Rows.Count <= 0)
        {
            sql = @"
                INSERT INTO [dbo].[Records]
                       ([target]
                       ,[account_sn])
                 VALUES
                       (@target
                       ,@account_sn)
        ";
            par.Clear();
            par.Add(new SqlParameter("@account_sn", account_sn));
            par.Add(new SqlParameter("@target", target));
            db.Execute(sql, par);
        }

    }
    public static string get_record(string target)
    {
        string account_sn = Global.get_account_sn();
        if (account_sn == "") return "0";
        
        //# 在抓資料庫
        Common.MSSQL_M db = new Common.MSSQL_M();
        ArrayList pars = new ArrayList();
        string sql = @"
                SELECT COUNT(target)
                FROM Records 
                WHERE target=@target 
                  AND account_sn=@account_sn
        ";
        pars.Clear();
        pars.Add(new SqlParameter("@account_sn", account_sn));
        pars.Add(new SqlParameter("@target", target));        
        int rtn = db.ExecuteScalar(sql, pars);
        return rtn.ToString();        
    }

    public static string get_account_sn()
    {
        if (HttpContext.Current.Session["ACCOUNT_SN"] == null)
        {
            return "";
        }
        return HttpContext.Current.Session["ACCOUNT_SN"].ToString();
    }
    public static string get_account_name()
    {
        if (HttpContext.Current.Session["NAME"] == null)
        {
            return "";
        }
        return HttpContext.Current.Session["NAME"].ToString();
    }
    public static void login(DataRow dr)
    {
        HttpContext.Current.Session["ACCOUNT_SN"] = dr["SN"].ToString();
        HttpContext.Current.Session["NAME"] = dr["NAME"].ToString();
    }
    public static void logout()
    {
        HttpContext.Current.Session["ACCOUNT_SN"] = null;
        HttpContext.Current.Session["NAME"] = null;
    }
}