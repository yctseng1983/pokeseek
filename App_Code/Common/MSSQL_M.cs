﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using MM.Database;
using System.Configuration;
using System.Data;
using System.Collections;

namespace Common
{
    /// <summary>
    /// MSSQL_M 的摘要描述
    /// </summary>
    public class MSSQL_M : MSSQL
    {
        public MSSQL_M()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;
            this.setConnectionString(connectionString);
        }
    }
}