﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Online 的摘要描述
/// </summary>
public class ChatRoom
{
    public ChatRoom()
	{
		//
		// TODO: 在這裡新增建構函式邏輯
		//
	}

    public string enter()
    {
        if (Global.get_account_sn() == "") { return "0"; }
        string sql = "";
        Common.MSSQL_M db = new Common.MSSQL_M();
        ArrayList par = new ArrayList();

        par.Add(new SqlParameter("@ACCOUNT_SN", Global.get_account_sn()));

        sql = @"DELETE FROM [dbo].[CHATROOM] WHERE ACCOUNT_SN=@ACCOUNT_SN";

        db.Execute(sql, par);
        sql = @"
            INSERT INTO [dbo].[CHATROOM]
                       ([ACCOUNT_SN]
                       ,[ENTER_TIME]
                       ,[LAST_UPDATE])
                 VALUES
                       (@ACCOUNT_SN
                       ,getdate()
                       ,getdate())        
        ";
        par.Clear();
        par.Add(new SqlParameter("@ACCOUNT_SN", Global.get_account_sn()));
        int r = db.Execute(sql, par);

        return r.ToString();
    }
    public string update()
    {
        string sql = "";
        Common.MSSQL_M db = new Common.MSSQL_M();
        ArrayList par = new ArrayList();

        sql = @"
            UPDATE [dbo].[CHATROOM] SET LAST_UPDATE=getdate() WHERE ACCOUNT_SN=@ACCOUNT_SN
        ";

        par.Add(new SqlParameter("@ACCOUNT_SN", Global.get_account_sn()));
        int r = db.Execute(sql, par);
        return r.ToString();
    }
    public string leave()
    {
        string sql = "";
        Common.MSSQL_M db = new Common.MSSQL_M();
        ArrayList par = new ArrayList();

        sql = @"
            DELETE [dbo].[CHATROOM] WHERE ACCOUNT_SN=@ACCOUNT_SN
        ";

        par.Add(new SqlParameter("@ACCOUNT_SN", Global.get_account_sn()));
        int r = db.Execute(sql, par);

        return r.ToString();
    }
    public string update_style(string style)
    {
        Common.MSSQL_M db = new Common.MSSQL_M();
        ArrayList par = new ArrayList();
        string ACCOUNT_SN = Global.get_account_sn();
        string sql = @"
                UPDATE [dbo].[CHATROOM] SET STYLE=@STYLE
                WHERE ACCOUNT_SN=@ACCOUNT_SN
        ";
        par.Add(new SqlParameter("@ACCOUNT_SN", ACCOUNT_SN));
        par.Add(new SqlParameter("@STYLE", style));
        int r = db.Execute(sql, par);

        return r.ToString();
    }
    public DataTable get_all_user_online()
    {

        string sql = "";
        Common.MSSQL_M db = new Common.MSSQL_M();

        sql = @"
                SELECT  dbo.ACCOUNT.NAME, dbo.ACCOUNT.ADDRESS,dbo.ACCOUNT.LON, dbo.ACCOUNT.LAT, dbo.ACCOUNT.SN,dbo.ACCOUNT.PICTURE,dbo.CHATROOM.STYLE,dbo.ACCOUNT.CREATION
                FROM dbo.ACCOUNT INNER JOIN
                    dbo.CHATROOM ON dbo.ACCOUNT.SN = dbo.CHATROOM.ACCOUNT_SN
                WHERE DATEDIFF(n,[LAST_UPDATE],getdate()) < 10
        ";
        DataTable dt = db.Select(sql);

        return dt;
    }
}