﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

public partial class resoures : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    /// <summary>
    /// 是否session掉了
    /// </summary>
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
    public static string isonline()
    {
        string account_sn = Global.get_account_sn();
        if (account_sn == "") return "";
        return account_sn;
    }

    /// <summary>
    /// 是否session掉了
    /// </summary>
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
    public static string getRain()
    {


        string url = "https://opendata.cwb.gov.tw/api/v1/rest/datastore/F-C0032-001?Authorization=rdec-key-123-45678-011121314";

        WebRequest myWebRequest = WebRequest.Create(url);

        WebResponse myWebResponse;

        myWebRequest.Timeout = 3000;

        myWebResponse = myWebRequest.GetResponse();

        //網頁的contentType
        string cType = myWebResponse.ContentType.ToString();

        Stream responseStream = myWebResponse.GetResponseStream();

        StreamReader reader1 = null;

        reader1 = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));

        //取出整個網頁
        string str = reader1.ReadToEnd();
        return str;
    }
}