﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
/*        txt_account.Attributes["placeholder"] = "請輸入英文字母與數字";
        txt_password.Attributes["placeholder"] = "請輸入英文字母與數字";
        txt_l_account.Attributes["placeholder"] = "請輸入英文字母與數字";
        txt_l_password.Attributes["placeholder"] = "請輸入英文字母與數字";*/
        if (!IsPostBack)
        {
            
            for (int i= DateTime.Now.Year; i>=1950;i--)
            {
                
                ddl_age.Items.Add(new ListItem(i.ToString(),i.ToString()));
            }
            ddl_city_SelectedIndexChanged(null,null);
        }
    }
    protected void btn_apply_Click(object sender, EventArgs e)
    {
        string account = txt_account.Text;
        string password = txt_password.Text;
        string email = txt_email.Text;
        string gender = ddl_gender.SelectedValue;
        string age = ddl_age.SelectedValue;
        string career = txt_career.Text;
        string school = txt_school.Text;
        string address = ddl_address.SelectedItem.Text;

        if (address == "請選擇") {
            MM.JSClass.run(@"MM.alert('請選擇居住地點');");
            return;
        }
        if (gender == "gender") gender = "";

        DataRow dr = get_account(account);

        if (dr != null)
        {
            MM.JSClass.run(@"
                MM.alert('帳號重複');
            ");
            return;
        }
        Common.MSSQL_M db = new Common.MSSQL_M();
        ArrayList par = new ArrayList();
        par.Add(new SqlParameter("@NAME", account));
        par.Add(new SqlParameter("@EMAIL", email));
        par.Add(new SqlParameter("@ACCOUNT", account));
        par.Add(new SqlParameter("@PASSWORD", password));
        par.Add(new SqlParameter("@PICTURE", ""));
        par.Add(new SqlParameter("@GENDER", gender));
        par.Add(new SqlParameter("@BIRTH_YEAR", age));
        par.Add(new SqlParameter("@CAREER", career));
        par.Add(new SqlParameter("@SCHOOL", school));
        par.Add(new SqlParameter("@ADDRESS", address));

        string sql = @"
                INSERT INTO [dbo].[ACCOUNT]
                           ([NAME]
                           ,[EMAIL]
                           ,[ACCOUNT]
                           ,[PASSWORD]
                           ,[PICTURE]
                           ,[GENDER]
                           ,[BIRTH_YEAR]
                           ,[CAREER]
                           ,[SCHOOL]
                           ,[ADDRESS])
                     VALUES
                           (@NAME
                           ,@EMAIL
                           ,@ACCOUNT
                           ,@PASSWORD
                           ,@PICTURE
                           ,@GENDER
                           ,@BIRTH_YEAR
                           ,@CAREER
                           ,@SCHOOL
                           ,@ADDRESS)
        ";
        int r = db.Execute(sql, par);
        if(r >= 1)
        {
            DataRow dr1 = get_account(account);
            Global.login(dr1);//通用變數註冊
            MM.JSClass.run(@"
                MM.alert('新增成功');
                setTimeout(function(){
                    location.href = 'villages/home/index.html';
                },1000);
            ");
        }
    }
    protected void btn_enter_Click(object sender, EventArgs e)
    {
        string account = txt_l_account.Text;
        string password = txt_l_password.Text;

        if(account == "" || password == "")
        {
            MM.JSClass.run(@"
                MM.alert('帳號/密碼錯誤');
            ");
            return;
        }

        bool r = isexist(account, password);

        if (r == false)
        {
            MM.JSClass.run(@"
                MM.alert('email或password錯誤');
            ");
            return;
        }
        DataRow dr = get_account(account);
        Global.login(dr);//通用變數註冊

        Response.Redirect("villages/home/index.html");
    }
    public bool isexist(string account, string password)
    {
        string sql = "";
        Common.MSSQL_M db = new Common.MSSQL_M();
        ArrayList par = new ArrayList();

        sql = @"SELECT * FROM ACCOUNT WHERE account=@account and password=@password";
        par.Add(new SqlParameter("@account", account));
        par.Add(new SqlParameter("@password", password));
        DataTable dt = db.Select(sql, par);

        if (dt.Rows.Count <= 0)
        {
            return false;
        }
        return true;
    }
    public DataRow get_account(string account)
    {
        string sql = "";
        Common.MSSQL_M db = new Common.MSSQL_M();
        ArrayList par = new ArrayList();

        sql = @"SELECT * FROM ACCOUNT WHERE account=@account";
        par.Add(new SqlParameter("@account", account));
        DataTable dt = db.Select(sql, par);

        if(dt.Rows.Count <= 0)
        {
            return null;
        }
        return dt.Rows[0];
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
    public static string get_user(string id, string email, string source)
    {
        Common.MSSQL_M db = new Common.MSSQL_M();
        ArrayList pars = new ArrayList();
        string sql = @"
                    SELECT *
                    FROM [ACCOUNT] WHERE id=@ID AND SOURCE=@SOURCE
        ";
        pars.Add(new SqlParameter("@ID", id));
        pars.Add(new SqlParameter("@SOURCE", source));
        DataTable dt = db.Select(sql, pars);


        //同時登入
        if (dt.Rows.Count >= 1)
        {
            DataRow dr = dt.Rows[0];
            Global.login(dr);//通用變數註冊
        }
        return dt.Rows.Count.ToString();
    }


    /// <summary>
    /// 由fb新增使用者
    /// </summary>
    /// <param name="email"></param>
    /// <returns></returns>
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
    public static string add_user(string source, string email, string first_name, string gender, string id,
        string last_name, string link, string locale, string pictureurl)
    {

        if (gender.ToLower() == "gender")
        {
            gender = "0";
        }
        else
        {
            gender = "1";
        }
        if (locale == "")
        {
            locale = "ENG";
        }

        //寫資料庫
        Common.MSSQL_M db = new Common.MSSQL_M();
        ArrayList pars = new ArrayList();
        string sql = @"
                        INSERT INTO [dbo].[ACCOUNT]
                                   ([SOURCE]
                                   ,[EMAIL]
                                   ,[NAME]
                                   ,[ACCOUNT]
                                   ,[ID]
                                   ,[LANG]
                                   ,[INP_DATE]
                                   ,[GENDER]
                                   ,[LINK]
                                   ,[LOCALE]
                                   ,[PICTURE]
                                   ,[VALID])
                             VALUES
                                   (@SOURCE
                                   ,@EMAIL
                                   ,@NAME
                                   ,@NAME
                                   ,@ID
                                   ,@LANG
                                   ,getdate()
                                   ,@GENDER
                                   ,@LINK
                                   ,@LOCALE
                                   ,@PICTUREURL
                                   ,@VALID)
        ";

        pars.Add(new SqlParameter("@SOURCE", source));
        pars.Add(new SqlParameter("@EMAIL", email));
        pars.Add(new SqlParameter("@NAME", last_name + first_name));
        pars.Add(new SqlParameter("@ID", id));
        pars.Add(new SqlParameter("@LANG", locale));
        pars.Add(new SqlParameter("@GENDER", gender));
        pars.Add(new SqlParameter("@LINK", link));
        pars.Add(new SqlParameter("@LOCALE", locale));
        pars.Add(new SqlParameter("@PICTUREURL", "account.png"));
        pars.Add(new SqlParameter("@VALID", "1"));

        string r = db.Execute(sql, pars).ToString();

        if (r == "1")
        {
            get_user(id, email, source);
        }
        return r;
    }
    protected void ddl_city_SelectedIndexChanged(object sender, EventArgs e)
    {
        string city = ddl_city.SelectedValue;
                //寫資料庫
        Common.MSSQL_M db = new Common.MSSQL_M();
        ArrayList pars = new ArrayList();
        string sql = @"
            SELECT *
              FROM [TOWN] where city_id5 =@city
        ";
        pars.Add(new SqlParameter("@city",city));
        DataTable dt = db.Select(sql, pars);

        ddl_address.Items.Clear();
        ddl_address.DataTextField = "TOWN5";
        ddl_address.DataSource = dt;
        ddl_address.DataBind();
    }
}