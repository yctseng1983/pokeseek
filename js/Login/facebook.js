﻿var Facebook = {
    login: function () {

        MM.mask.show();
        FB.login(function (response) {
            if (response.authResponse) {
                // Get and display the user profile data
                Facebook.getUserData();
            } else {


                MM.mask.hide();
            }
        }, { scope: 'email' });

        return false;
    },
    getUserData: function () {

        FB.api('/me', { fields: 'id,first_name,last_name,email,link,gender,locale,picture' },
        function (fbuser) {

            fbuser.pictureurl = fbuser.picture.data.url;
            fbuser.source = "facebook";
            //user
            MM.ajax.url = "Login.aspx/get_user";
            MM.ajax.timeout = 20 * 1000;
            MM.ajax.params = fbuser;
            MM.ajax.success = function (m) {

                if (m == '0') {
                    Facebook.add_user(fbuser);
                } else {
                    MM.mask.hide();
                    MM.alert('登入成功');

                    setTimeout(function () {
                        location.href = 'welcome.html';
                    }, 500);
                    
                }

            };
            MM.ajax.error = function (e) {
                MM.alert('資料寫入失敗');
                MM.mask.hide();
            }
            MM.ajax.web_method();
        });
    },
    add_user: function (fbuser) {
        fbuser.source = 'facebook';
        //user
        MM.ajax.url = "Login.aspx/add_user";
        MM.ajax.timeout = 20 * 1000;
        MM.ajax.params = fbuser;
        MM.ajax.success = function (m) {

            if (m == '1') {
                MM.mask.hide();
                MM.alert('登入成功');

                setTimeout(function () {
                    location.href = 'welcome.html';
                }, 500);
            } else {
                MM.alert('資料寫入失敗');
                MM.mask.hide();
            }
        };
        MM.ajax.error = function (e) {
            MM.alert('資料寫入失敗');
            MM.mask.hide();
        }
        MM.ajax.web_method();
    },
    logout: function () {
        FB.logout(function () {
        });
    }
};
window.fbAsyncInit = function () {
    // FB JavaScript SDK configuration and setup
    FB.init({
        appId: '163422111103460', // FB App ID
        cookie: true,  // enable cookies to allow the server to access the session
        xfbml: true,  // parse social plugins on this page
        version: 'v2.11' // use graph api version 2.8
    });

    // Check whether the user already logged in
    FB.getLoginStatus(function (response) {
        if (response.status === 'connected') {
            //display user data
            //getFbUserData();
        }
    });
};

// Load the JavaScript SDK asynchronously
(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));