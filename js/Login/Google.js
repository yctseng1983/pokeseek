﻿var Google = {
    GoogleAuth: null,
    SCOPE: 'profile',
    init: function () {
        gapi.load('client:auth2', Google.initClient);
    },
    initClient: function () {

        // Retrieve the discovery document for version 3 of Google Drive API.
        // In practice, your app can retrieve one or more discovery documents.
        var discoveryUrl = 'https://www.googleapis.com/discovery/v1/apis/drive/v3/rest';

        // Initialize the gapi.client object, which app uses to make API requests.
        // Get API key and client ID from API Console.
        // 'scope' field specifies space-delimited list of access scopes.
        gapi.client.init({
            'apiKey': 'AIzaSyCShaReWA7uzTboYS9Ys5dDck9J2uN-Nos',
            'discoveryDocs': ['https://www.googleapis.com/discovery/v1/apis/drive/v3/rest'],
            'clientId': '1056055896435-nvvlp331tjiechmoqfclobauce54ipeu.apps.googleusercontent.com',
            'scope': Google.SCOPE
        }).then(function () {
            Google.GoogleAuth = gapi.auth2.getAuthInstance();

            if (Google.GoogleAuth.isSignedIn.get()) {
                Google.GoogleAuth.signOut();
            }

            // Listen for sign-in state changes.
            Google.GoogleAuth.isSignedIn.listen(Google.updateSigninStatus);

        });
    },
    handleAuthClick:function() {
        if (Google.GoogleAuth.isSignedIn.get()) {
            Google.GoogleAuth.signOut();
        } else {
            Google.GoogleAuth.signIn();
        }
        return false;
    },
    updateSigninStatus: function () {
        Google.setSigninStatus();
    },
    setSigninStatus: function () {
        var user = Google.GoogleAuth.currentUser.get();
        var isAuthorized = user.hasGrantedScopes(Google.SCOPE);
        if (isAuthorized) {
            var guser = {
                source: 'google',
                email: user.w3.U3,
                first_name: user.w3.ofa,
                gender:'',
                id: user.El,
                last_name: user.w3.wea,
                link: '',
                locale: '',
                pictureurl:user.w3.Paa
            }
            //user
            MM.ajax.url = "Login.aspx/get_user";
            MM.ajax.timeout = 20 * 1000;
            MM.ajax.params = guser;
            MM.ajax.success = function (m) {

                if (m == '0') {
                    Google.add_user(guser);
                } else {
                    location.href = 'default.aspx';
                }

            };
            MM.ajax.error = function (e) {
                MM.alert('資料寫入失敗');
                MM.mask.hide();
            }
            MM.ajax.web_method();
        } else {
            Google.GoogleAuth.signOut();
        }
    },
    add_user: function (user) {
        user.source = 'google';
        //user
        MM.ajax.url = "Login.aspx/add_user";
        MM.ajax.timeout = 20 * 1000;
        MM.ajax.params = user;
        MM.ajax.success = function (m) {

            if (m == '1') {
                LangDDL.manual(user);
            } else {
                MM.alert('資料寫入失敗');
                MM.mask.hide();
            }
        };
        MM.ajax.error = function (e) {
            MM.alert('資料寫入失敗');
            MM.mask.hide();
        }
        MM.ajax.web_method();
    }
};