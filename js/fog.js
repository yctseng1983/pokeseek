var fogParticleUrl = "img/fog.png";//物件-霧
var fogGen = setInterval(function(){generateFogParticle();},30);//300

var pCount = 0;
var fromMiddleStart = false;
function generateFogParticle()
{
    pCount++;
    if(pCount % 3 == 0){
        var spn = Math.floor(Math.random() * (300 - 0)) + 0;
        var w = Math.floor(Math.random() * (400 - 200)) + 200;
        var h = Math.floor(Math.random() * (300 - 150)) + 100;
        var t = Math.floor(Math.random() * (40000 - 30000)) + 4000;
        if (fromMiddleStart == true) {

            t = Math.floor(Math.random() * (40000 - 30000)) + 10000;
            w = Math.floor(Math.random() * (400 - 200)) + 600;

            fromMiddleStart = false;
        } else {
            fromMiddleStart = true;
        }
        $("#fog").append("<img src='" + fogParticleUrl + "' class='fog-particle' id='particle-" + pCount + "'>");
        var p = $("#particle-" + pCount);
        p.css("width", w);
        p.css("height", h);
        p.css("bottom", "" + spn + "px");
        p.animate({ opacity: 0.1 }, 1500);
        p.animate({ left: '-150px' }, t, "linear", function () {
            $(this).remove();
        });
    }


}
