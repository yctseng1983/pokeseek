﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class selecthome : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string account_sn = Global.get_account_sn();
        if (account_sn == "")
        {
            MM.JSClass.run(@"
            MM.alert('請重新登入');
            setTimeout(function(){
                location.href = 'login.aspx';
            },1000);
        ");
        }
    }
    protected void btn_apply_Click(object sender, EventArgs e)
    {

        string lon = hid_lon.Value;
        string lat = hid_lat.Value;
        string account_sn = Global.get_account_sn();

        string sql = "";
        Common.MSSQL_M db = new Common.MSSQL_M();
        ArrayList par = new ArrayList();


        par.Clear();
        sql = @"
            UPDATE ACCOUNT SET LON=@LON,LAT=@LAT WHERE SN=@ACCOUNT_SN
        ";
        par.Add(new SqlParameter("@LON", lon));
        par.Add(new SqlParameter("@LAT", lat));
        par.Add(new SqlParameter("@ACCOUNT_SN", account_sn));
        int r = db.Execute(sql, par);

        MM.JSClass.run(@"
            MM.alert('申請成功,自動轉至登入頁面');
            setTimeout(function(){
                location.href = 'default.aspx';
            },1000);
        ");
    }
}