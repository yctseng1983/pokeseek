﻿var total_images = $("body img").length;
var images_loaded = 0;
$("body").find('img').each(function () {
    var fakeSrc = $(this).attr('src');
    $("<img/>").attr("src", fakeSrc).css('display', 'none').load(function () {
        images_loaded++;
        if (images_loaded >= total_images) {
            // now all images are loaded.
            setTimeout(function () {
                $('#mask').hide();
            }, 500);
            
        }
    });

});


$(function () {

    Web.init();
});

var Web = {
    
    scrollkey: [false, false, false],//解鎖三個關卡的key
    scrollSel: [null,null,null],//選擇的結果
    scrolltip:[false,false,false],
    init: function () {
        MM.cookie.set('ugame_key_count', 0);
        $('.btn_FF').hide();
        //# scroll event
        document.body.onscroll = Web.onscroll;
        //# 1st choose
        $('.n95').click(function () {
            $('.n95').fadeOut();
            $('.fan').fadeOut();
            Web.scrollkey[0] = true;
            Web.scrollSel[0] = 0;
            $('.user_box li')[0].style.display = '';
            MM.cookie.set('ugame_key_count', 1);
        });
        $('.fan').click(function () {
            $.notify('啊，好像另外一個比較適合', {
                className: "",
                globalPosition: 'top center',
                autoHideDelay: 3 * 1000
            });
            $('.notifyjs-corner').css('top', '10%');
            $('.notifyjs-corner').css('left', '50%');
            $('.notifyjs-corner').css('margin-left', '-125px');
            return;
            $('.n95').fadeOut();
            $('.fan').fadeOut();
            Web.scrollkey[0] = true;
            Web.scrollSel[0] = 1;
            $('.user_box li')[1].style.display = '';
        });
        //# 2nd choose
        $('.switch').click(function () {
            $('.switch').fadeOut();
            Web.scrollkey[1] = true;
            $('.user_box li')[1].style.display = '';
            $('.btn_DD').find("img").eq(0).hide();//hide leak electrosity
            //cookie
            var count = MM.cookie.get('ugame_key_count');
            count = parseInt(count);
            count++;
            MM.cookie.set('ugame_key_count', count);
        });
        //# 3rd choose
        $('.btn_object_seed').click(function () {
            $('.btn_object_seed').fadeOut();
            $('.audio_unbrella').fadeOut();
            $('.btn_FF').fadeIn();
            Web.scrollkey[2] = true;
            Web.scrollSel[2] = 0;
            $('.user_box li')[2].style.display = '';
            //cookie
            var count = MM.cookie.get('ugame_key_count');
            count = parseInt(count);
            count++;
            MM.cookie.set('ugame_key_count', count);
        });
        $('.audio_unbrella').click(function () {
            $.notify('再想一下，哪個是最能解決問題的方法?', {
                className: "",
                globalPosition: 'bottom center',
                autoHideDelay: 3 * 1000
            });
            $('.notifyjs-bootstrap-base').addClass('notifyjs-bootstrap-base1');
            $('.notifyjs-corner').css('left', '50%');
            $('.notifyjs-corner').css('margin-left', '-180px');
            $('.notifyjs-corner').css('bottom', '13%');
            return;
            $('.btn_object_seed').fadeOut();
            $('.audio_unbrella').fadeOut();
            Web.scrollkey[2] = true;
            Web.scrollSel[2] = 1;
        });
    },
    onscroll: function () {
        var p = this.pageYOffset;
        
        if (p >= 2100 && p <= 2800) {
            if (Web.scrollkey[0] == false){
                window.scrollTo(0, 2100);
                if(Web.scrolltip[0] == false){
                    //MM.notify.info('請選擇[口罩|風扇]?', 'top center');
                    Web.scrolltip[0] = true;
                }
            }
        }
        if (p >= 3000 && p <= 3800) {
            if (Web.scrollkey[1] == false) {
                window.scrollTo(0, 3000);
                if (Web.scrolltip[1] == false){
                    //MM.notify.info('請轉動xxxx', 'top center');
                    Web.scrolltip[1] = true;
                }
            }
        }
        if (p >= 4300 && p <= 5700) {
            if (Web.scrollkey[2] == false) {
                window.scrollTo(0, 4300);
                if (Web.scrolltip[2] == false) {
                    //MM.notify.info('請選擇[種子|雨傘]?', 'top center');
                    Web.scrolltip[2] = true;
                }
            }
        }
        if (p >= 3900 && Web.scrolltip[2] == true && Web.scrollSel[2] == 0) {
            //$('.btn_FF').show();
        }
    }
}