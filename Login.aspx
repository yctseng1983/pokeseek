﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="login" %>

<!DOCTYPE>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/layout_login.css">

    <title>uGame</title>

    <style>
        input.register, input.login{ height:36px; line-height:36ppx; border:0px; font-size:18px; font-weight:600; color:#fff;}
        input.register{ width:70%; 
        background: #fd6411; /* Old browsers */
        background: -moz-linear-gradient(top, #fd6411 0%, #f90a05 100%); /* FF3.6-15 */
        background: -webkit-linear-gradient(top, #fd6411 0%,#f90a05 100%); /* Chrome10-25,Safari5.1-6 */
        background: linear-gradient(to bottom, #fd6411 0%,#f90a05 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fd6411', endColorstr='#f90a05',GradientType=0 ); /* IE6-9 */}
        input.register:hover{
        background: #ef5e10; /* Old browsers */
        background: -moz-linear-gradient(top, #ef5e10 0%, #d60606 100%); /* FF3.6-15 */
        background: -webkit-linear-gradient(top, #ef5e10 0%,#d60606 100%); /* Chrome10-25,Safari5.1-6 */
        background: linear-gradient(to bottom, #ef5e10 0%,#d60606 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ef5e10', endColorstr='#d60606',GradientType=0 ); /* IE6-9 */}

        input.login{ width:100%;
        background: #127730; /* Old browsers */
        background: -moz-linear-gradient(top, #127730 0%, #02611e 100%); /* FF3.6-15 */
        background: -webkit-linear-gradient(top, #127730 0%,#02611e 100%); /* Chrome10-25,Safari5.1-6 */
        background: linear-gradient(to bottom, #127730 0%,#02611e 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#127730', endColorstr='#02611e',GradientType=0 ); /* IE6-9 */}
        input.login:hover{
        background: #106825; /* Old browsers */
        background: -moz-linear-gradient(top, #106825 1%, #025418 100%); /* FF3.6-15 */
        background: -webkit-linear-gradient(top, #106825 1%,#025418 100%); /* Chrome10-25,Safari5.1-6 */
        background: linear-gradient(to bottom, #106825 1%,#025418 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#106825', endColorstr='#025418',GradientType=0 ); /* IE6-9 */}


        #UpdatePanel1, #UpdatePanel2 { 
                width: 200px;
            border:0px solid #000;
        }
    </style>


    <script src="js/jquery.min.js"></script>
    <script src="js/MMJS/utility.js"></script>
    <script src="js/MMJS/lightbox.js"></script>
    <script src="js/MMJS/mask.js"></script>
    <script src="js/MMJS/ajax.js"></script>
    <script src="js/MMJS/cookie.js"></script>
    <script src="js/Login/facebook.js"></script>
    <script src="js/Login/Google.js"></script>
</head>


<body class="bg_zoom">
    <form id="form1" runat="server">
        <div class="wrapper_login">
            <a href="default.aspx" class="white_block_show form_close"></a>
            <div class="form_area">
                <div class="form_registered">
                    <div>
                        <label for="username">帳號</label>
                        <asp:TextBox ID="txt_account" Text="" ClientIDMode="Static" runat="server" placeholder="請輸入您的帳號"></asp:TextBox>
                    </div>
                    <div>
                        <label for="password">密碼</label>
                        <asp:TextBox TextMode="Password" ID="txt_password" Text="" ClientIDMode="Static" runat="server" placeholder="請輸入密碼"></asp:TextBox>

                    </div>
                    <div>
                        <label for="email">電子信箱</label>
                        <asp:TextBox ID="txt_email" Text="" ClientIDMode="Static" runat="server" placeholder="請輸入Email"></asp:TextBox>

                    </div>
                    <div>
                        <label for="sex" class="sex_text">性別</label>
                        <asp:DropDownList ID="ddl_gender" runat="server" ClientIDMode="Static">
                            <asp:ListItem Value="9">請選擇</asp:ListItem>
                            <asp:ListItem Value="1">男</asp:ListItem>
                            <asp:ListItem Value="0">女</asp:ListItem>
                        </asp:DropDownList>
                        <label for="age" class="age_text" style="font-size:16px;">出生年</label>
                        <asp:DropDownList ID="ddl_age" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div>
                        <label for="joy">職業</label>
                        <asp:TextBox ID="txt_career" runat="server" ClientIDMode="Static" placeholder="請輸入您的職業"></asp:TextBox>

                    </div>
                    <div>
                        <label for="school">畢業學校</label>
                        <asp:TextBox ID="txt_school" runat="server" ClientIDMode="Static" placeholder="請輸入您的學校"></asp:TextBox>

                    </div>
                    <div>
                        <label for="address">居住地點</label>

                        <asp:ScriptManager id="ScriptManager1" runat="server">
                         </asp:ScriptManager>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                
                                <asp:DropDownList ID="ddl_city" width="80" AutoPostBack="true" OnSelectedIndexChanged="ddl_city_SelectedIndexChanged" runat="server" ClientIdMode="Static">
                                    <asp:ListItem Value="E">高雄</asp:ListItem>
                                    <asp:ListItem Value="D">台南</asp:ListItem>
                            
                                </asp:DropDownList>
                        
                                <asp:DropDownList ID="ddl_address" Width="100" runat="server" ClientIDMode="Static">
                                    
                                </asp:DropDownList>
                                
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="btn">
                        <asp:Button ID="btn_apply" runat="server" CssClass="register" ClientIDMode="Static" Text="註冊" OnClientClick="return check_apply();" OnClick="btn_apply_Click" />

                    </div>
                </div>
                <div class="form_login">
                    <div class="user_img">
                        <img src="img/loading/logo.gif">
                        <!--棲居大頭照-->
                    </div>
                    <div>
                        <asp:TextBox ID="txt_l_account" runat="server" placeholder="帳號" ClientIDMode="Static"></asp:TextBox>
                    </div>
                    <div>
                        <asp:TextBox TextMode="Password" ID="txt_l_password" runat="server" ClientIDMode="Static" placeholder="密碼"></asp:TextBox>
                    </div>
                    <div class="btn">
                        <asp:Button ID="btn_enter" runat="server" CssClass="login" Text="登入" OnClientClick="return login();" OnClick="btn_enter_Click" />
                        <a href="#">忘記密碼</a>
                    </div>
                    <p>以其他帳號登入</p>
                    <div class="other_login">
                        <a href="javascript:void(0);" onclick="return Facebook.login();"><i class="fa fa-4x fa-facebook-square" aria-hidden="true"></i></a>
                        <a style="display: none;" href="javascript:void(0);" onclick="return Google.handleAuthClick();"><i class="fa fa-4x fa-google-plus-square" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>

            <svg opacity="0" class="white_block_show" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="272 105 1390 985" xml:space="preserve">
                <!-- preserveAspectRatio="xMinYMin meet" -->
                <g opacity="0.7" class="white_block_01">
                    <path fill="#FFFFFF" d="M497.542,208.101c-39.434,12.957-81.086,68.539-100.11,103.988c-31.237,58.208-61.67,117.26-82.644,180.07
		c-19.514,58.436-36.921,129.071-13.734,188.854c17.801,45.896,37.236,90.007,51.33,137.321
		c14.411,48.378,27.577,97.525,50.684,142.677c31.63,61.808,77.901,84.568,145.961,86.015c60.742,1.291,120.568-3.74,180.951-9.375
		c31.697-2.957,59.713-7.226,91.903-7.552c28.188-0.285,55.033-10.036,83.143-14.121c99.1-14.401,191.653-38.395,288.573,1.035
		c28.744,11.693,56.918,10.277,87.125,13.491c79.744,8.484,172.334,17.245,242.338-30.386
		c66.104-44.978,92.943-130.179,95.686-205.447c3.275-89.962-1.215-184.646-20.047-272.776
		c-8.449-39.542-19.559-78.64-34.424-116.27c-8.676-21.961-20.697-42.684-28.43-64.981c-8.127-23.433-9.668-48.699-19.234-71.706
		c-9.777-23.517-23.604-45.053-43.469-61.291c-45.947-37.563-111.996-44.206-168.811-49.883
		c-68.814-6.873-137.967-9.653-206.768-16.808c-69.238-7.198-137.99-18.83-207.354-24.811c-38.781-3.345-77.709-4.557-116.603-2.554
		c-46.459,2.393-107.421-1.897-146.063,28.943c-37.988,30.322-93.527,26.633-126.007,61.563" />
                </g>
                <g opacity="0.7" class="white_block_02">
                    <path fill="#FFFFFF" d="M1429.554,951.755c39.434-12.957,81.086-68.539,100.109-103.988c31.236-58.208,61.67-117.26,82.645-180.069
		c19.514-58.436,36.92-129.071,13.732-188.855c-17.801-45.896-37.234-90.007-51.328-137.321
		c-20.361-68.354-39.461-167.374-105.125-208.477c-37.906-23.727-88.457-19.624-131.17-18.55
		c-119.816,3.012-230.314-8.244-348.521,9.667c-94.151,14.267-185.259,35.923-278.064,17.524
		c-76.113-15.091-179.772-1.574-257.316,12.55c-41.432,7.545-99.178,88.925-127.566,120.504
		c-60.67,67.489,9.839,157.46,12.948,243.771c2.369,65.787-25.27,55.023-11.502,119.451c8.45,39.541,19.559,78.639,34.424,116.269
		c8.676,21.961,20.697,42.685,28.43,64.981c8.127,23.433,9.668,48.7,19.234,71.707c9.778,23.516,23.605,45.052,43.47,61.291
		c45.946,37.563,111.995,44.206,168.81,49.882c68.814,6.874,137.968,9.654,206.769,16.808
		c69.238,7.198,139.058,46.755,208.419,52.734c38.783,3.344,76.643-23.368,115.535-25.371
		c46.459-2.392,107.422,1.898,146.063-28.942c37.988-30.322,93.527-26.634,126.008-61.563" />
                </g>
            </svg>

        </div>
        <asp:HiddenField ID="hid_ugame_key_count" ClientIDMode="Static" runat="server" />
    </form>
</body>
</html>

<script async defer src="https://apis.google.com/js/api.js"
    onload="this.onload=function(){};Google.init();"
    onreadystatechange="if (this.readyState === 'complete') this.onload()">
</script>
<script>
    $(function () {
        var ugame_key_count = MM.cookie.get('ugame_key_count');
        document.getElementById('hid_ugame_key_count').value = ugame_key_count;
    });
    function login() {
        var account = $('#txt_l_account').val();
        var password = $('#txt_l_password').val();

        var r = "";
        if (account == '') {
            r += '請輸入帳號<br/>';
        }
        if (password == '') {
            r += '請輸入密碼<br/>';
        }
        if (r != '') {
            MM.alert(r);
            return false;
        } else {
            return true;
        }
    }
    function check_apply() {
        var account = $('#txt_account').val();
        var password = $('#txt_password').val();
        var email = $('#txt_email').val();

        var r = "";
        if (account == '') {
            r += '請輸入帳號<br/>';
        }
        if (password == '') {
            r += '請輸入密碼<br/>';
        }
        if (email == '') {
            r += '請輸入E-mail<br/>';
        }
        if (r != '') {
            MM.alert(r);
            return false;
        } else {
            return true;
        }

    }
</script>
