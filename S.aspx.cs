﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class S : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    /// <summary>
    /// 是否session掉了
    /// </summary>
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
    public static string isonline()
    {
        string account_sn = Global.get_account_sn();
        if (account_sn == "") return "";
        return account_sn;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
    public static string get_name()
    {
        string account_name = Global.get_account_name();
        if (account_name == "") return "";
        return account_name;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
    public static string record(string id)
    {
        Global.record(id);

        return "";
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
    public static string get_record(string id)
    {        
        return Global.get_record(id);        
    }
}