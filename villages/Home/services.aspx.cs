﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class services : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }



    /// <summary>
    /// 進入聊天室
    /// </summary>
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string enter()
    {
        ChatRoom cr = new ChatRoom();
        return cr.enter();
    }
    /// <summary>
    /// 離開聊天室
    /// </summary>
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
    public static string update()
    {
        ChatRoom cr = new ChatRoom();
        return cr.update();
    }
    /// <summary>
    /// 離開聊天室
    /// </summary>
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
    public static string leave()
    {
        ChatRoom cr = new ChatRoom();
        return cr.leave();
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
    public static string set_chatroom_style(string style)
    {
        ChatRoom cr = new ChatRoom();
        return cr.update_style(style);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string get_all_user_online()
    {
        ChatRoom cr = new ChatRoom();
        return DataTableToJsonWithJavaScriptSerializer(cr.get_all_user_online());
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
    public static string get_account()
    {

        string ACCOUNT_SN = Global.get_account_sn();

        if (ACCOUNT_SN == "")
        {
            return "";
        }

        //# 在抓資料庫
        Common.MSSQL_M db = new Common.MSSQL_M();
        ArrayList par = new ArrayList();
        string sql = @"
           SELECT top 1 * FROM ACCOUNT WHERE SN=@SN
        ";
        par.Add(new SqlParameter("@SN", ACCOUNT_SN));
        return DataTableToJsonWithJavaScriptSerializer(db.Select(sql,par));
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
    public static DataTable get_chats()
    {
        try
        {
            Common.MSSQL_M db = new Common.MSSQL_M();
            ArrayList par = new ArrayList();
            string sql = @"
                SELECT A.*,B.name,B.lon,B.lat FROM 
                    CHAT A,ACCOUNT B 
                WHERE A.ACCOUNT_SN = B.SN 
                    AND DATEDIFF(hh,[time],getdate()) <= 3
                order by TIME 
            ";
            DataTable dt = db.Select(sql);

            if(dt.Rows.Count <= 0)
            {
                sql = @"
                    SELECT top 10 A.*,B.name,B.lon,B.lat FROM 
                        CHAT A,ACCOUNT B 
                    WHERE A.ACCOUNT_SN = B.SN 
                    order by TIME desc
                ";
                dt = db.Select(sql);
            }

            return dt;
        }
        catch (Exception ee){

        }
        return null;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
    public static DataTable get_infowindows()
    {
        try
        {
            Common.MSSQL_M db = new Common.MSSQL_M();
            ArrayList par = new ArrayList();
            string sql = @"
                SELECT c.*,a.name,a.LON,a.LAT
                  FROM [dbo].[CHAT] c, [ACCOUNT] a
                  Where c.ACCOUNT_SN = a.SN and c.EndInfoWindowTime >= getdate()
            ";
            DataTable dt = db.Select(sql);

            return dt;
        }
        catch (Exception ee)
        {

        }
        return null;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
    public static DataTable get_spots()
    {

        //# 在抓資料庫
        Common.MSSQL_M db = new Common.MSSQL_M();
        ArrayList par = new ArrayList();
        string sql = @"
            SELECT * FROM SPOT
        ";
        return db.Select(sql);
    }
    /// <summary>
    // 
    /// </summary>
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static int add_chat(string text, string endtime,string style)
    {

        string ACCOUNT_SN = Global.get_account_sn();
        //# 防呆
        if(ACCOUNT_SN == "")
        {
            return 0;
        }
        //# 在抓資料庫
        Common.MSSQL_M db = new Common.MSSQL_M();
        ArrayList par = new ArrayList();
        string sql = @"
            INSERT INTO [dbo].[CHAT]
                       ([TIME]
                       ,[EndInfowindowTime]
                       ,[message]
                       ,[style]
                       ,[ACCOUNT_SN])
                 VALUES
                       (getdate()
                       ,@EndInfowindowTime
                       ,@message
                       ,@style
                       ,@ACCOUNT_SN)
        ";
        par.Add(new SqlParameter("@EndInfowindowTime", endtime));
        par.Add(new SqlParameter("@message",text));
        par.Add(new SqlParameter("@style", style));
        par.Add(new SqlParameter("@ACCOUNT_SN", ACCOUNT_SN));
        int r = db.Execute_Return_Identity(sql,par);

        return r;
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
    public static string getRain()
    {


        string url = "https://opendata.cwb.gov.tw/api/v1/rest/datastore/F-C0032-001?Authorization=rdec-key-123-45678-011121314";

        WebRequest myWebRequest = WebRequest.Create(url);

        WebResponse myWebResponse;

        myWebRequest.Timeout = 3000;

        myWebResponse = myWebRequest.GetResponse();

        //網頁的contentType
        string cType = myWebResponse.ContentType.ToString();

        Stream responseStream = myWebResponse.GetResponseStream();

        StreamReader reader1 = null;

        reader1 = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));

        //取出整個網頁
        string str = reader1.ReadToEnd();
        return str;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
    public static string getUltraviolet()
    {


        string url = "http://opendata.epa.gov.tw/webapi/Data/UV/?$orderby=PublishTime%20desc&$skip=0&$top=1000&format=json";

        WebRequest myWebRequest = WebRequest.Create(url);

        WebResponse myWebResponse;

        myWebRequest.Timeout = 3000;

        myWebResponse = myWebRequest.GetResponse();

        //網頁的contentType
        string cType = myWebResponse.ContentType.ToString();

        Stream responseStream = myWebResponse.GetResponseStream();

        StreamReader reader1 = null;

        reader1 = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));

        //取出整個網頁
        string str = reader1.ReadToEnd();
        return str;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
    public static string getAQI()
    {


        string url = "http://opendata.epa.gov.tw/webapi/Data/REWIQA/?$orderby=SiteName&$skip=0&$top=1000&format=json";

        WebRequest myWebRequest = WebRequest.Create(url);

        WebResponse myWebResponse;

        myWebRequest.Timeout = 3000;

        myWebResponse = myWebRequest.GetResponse();

        //網頁的contentType
        string cType = myWebResponse.ContentType.ToString();

        Stream responseStream = myWebResponse.GetResponseStream();

        StreamReader reader1 = null;

        reader1 = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));

        //取出整個網頁
        string str = reader1.ReadToEnd();
        return str;
    }
    public static string DataTableToJsonWithJavaScriptSerializer(DataTable table)
    {
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
        Dictionary<string, object> childRow;
        foreach (DataRow row in table.Rows)
        {
            childRow = new Dictionary<string, object>();
            foreach (DataColumn col in table.Columns)
            {
                childRow.Add(col.ColumnName, row[col]);
            }
            parentRow.Add(childRow);
        }
        return jsSerializer.Serialize(parentRow);
    }
}