﻿
function initMap() {

    Online.init();

    Chatbox.init();

    History.init();

    Weather.init();
    
    Googlemap.init(function () {

        Me.init();
        Spot.init(Googlemap.map);

        House.init(function () {
            Infowindow.init();
        });
    });
}
var Me = {
    user:null,
    color: '',
    init: function () {
        //燈光選顏色
        $('[name="me-light"]').paletteColorPicker({
            clear_btn: 'last',
            close_all_but_this: true, 
            onchange_callback: function (clicked_color) {
                Me.color = clicked_color;
                Me.set_style();
            }
        });
        //取得個人資料
        query('get_account', null, function (users) {

            if (users.length >= 1) {
                var user = users[0];
                Me.user = user;
                Googlemap.map.setCenter(House.get_position(user));
            }
        });
    },
    set_style: function (v) {

        var effect = $('#slt-effects option:selected').val();
        var light = Me.color;
        var athome = $('#stl-me-status option:selected').eq(0).val();

        if (athome == false) {
            effect = '';
        }
        var o = {
            effect: effect,
            light: light,
            athome: athome
        }

        o = JSON.stringify(o);
        o = escape(o);

        query('set_chatroom_style', { style: o }, function () {
            House.getData();
        }, function () {

        });

        if (athome == false) {
            $('#slt-effects').prop("disabled", true);
        } else {
            $('#slt-effects').prop("disabled", false);
        }
    }
}
var Weather = {
    init: function () {

        this.rain();
        this.ultraviolet();
        this.aqi();
    },
    rain: function () {
        var ajax = $.ajax({
            url: "services.aspx/getRain",
            type: "POST",
            data: "",
            dataType: 'text',
            contentType: "application/json; charset=utf-8",
            timeout: 10 * 1000,
            success: function (str) {
                var json = $.parseJSON(str);

                for (var i = 0; i < json.records.location.length; i++) {
                    var l = json.records.location[i];
                    if (l.locationName.indexOf('高雄市') >= 0) {
                        for (var j = 0; j < l.weatherElement.length; j++) {
                            var e = l.weatherElement[j];

                            //降雨機率
                            if (e.elementName.toLowerCase() == 'wx') {
                                var text = e.time[0].parameter.parameterName;

                                //$('#rain-percent').html(text);
                            }
                            //降雨機率
                            if (e.elementName.toLowerCase() == 'pop') {

                                var percent = e.time[0].parameter.parameterName;
                                percent = parseInt(percent);

                                $('#rain-percent').html(percent + '% ' + $('#rain-percent').html());
                                var image = Math.ceil(percent / 20);
                                image++;
                                if (image <= 0) image = 1;
                                if (image >= 6) image = 5;
                                //$('#rain').attr('xlink:href', 'img/animation/rain_Lv' + image + '.gif')
                            }
                            //氣溫
                            if (e.elementName.toLowerCase() == 'mint') {

                                var temp = e.time[0].parameter.parameterName;


                                $('#temperature').html(temp + '°C');
                            }

                        }
                    }
                }
            },
            error: function () { }
        });
    },
    ultraviolet: function () {

        var ajax = $.ajax({
            url: "services.aspx/getUltraviolet",
            type: "POST",
            data: "",
            dataType: 'text',
            contentType: "application/json; charset=utf-8",
            timeout: 10 * 1000,
            success: function (str) {
                var json = $.parseJSON(str);

                json.forEach(function (item) {
                    if (item.County == '高雄市') {
                        var uvi = parseInt(item.UVI);
                        var r = '';
                        if (uvi <= 2) r = '低量級';
                        if (uvi == 3) r = '中量級';
                        if (uvi >= 4) r = '高量級';
                        $('#ultraviolet').html(r+' '+uvi);
                    }
                });
            },
            error: function () { }
        });
    },
    aqi: function () {
        var ajax = $.ajax({
            url: "services.aspx/getAQI",
            type: "POST",
            data: "",
            dataType: 'text',
            contentType: "application/json; charset=utf-8",
            timeout: 10 * 1000,
            success: function (str) {
                var json = $.parseJSON(str);

                var items = [];
                json.forEach(function (item) {
                    if (item.County == '高雄市') {
                        items.push(item);
                    }
                });

                //show examples: 普通 AQI86
                var index = Math.floor((Math.random() * items.length - 1) + 1);
                var item = items[index];
                var r = '';
                
                r = item.Status + ' AQI ' + item.AQI;
                $('#aqi').html(r);
            },
            error: function () { }
        });
    },
}
var Chatbox = {
    face:'1',    
    init: function () {
        //# 檢查有沒有登入
        $('#chat-text').click(function () {
            if (Me.user == null) {
                MM.alert('一起加入社群聊天，請<a href="../../login.aspx">登入</a>或<a href="../../login.aspx">註冊</a>‧');
            }
        });
        $('#chat-text').keypress(function (e) {
            if (e.which == 13) {
                //Chatbox.send();
            }
        });
        //選取moji face
        $('#tab-demo a').click(function () {
            Chatbox.facee = $(this).attr('value');
        });
        
        //文字選顏色
        $('[name="box-light"]').paletteColorPicker({
            clear_btn: 'last',
            close_all_but_this: true,
            onchange_callback: function (clicked_color) {
            }
        });
        //選表情
        $('.tab-inner a').click(function () {
            Chatbox.face = $(this).attr('value');
        });
    },
    send: function () {
        var text = document.getElementById('chat-text').value;
        if (text.length <= 0) {
            MM.alert('你想發表什麼?');
            return;
        }

        //取得存活時間
        var livetime = $('#slt-livetime option:selected').eq(0).val();
        livetime = parseInt(livetime);
        var endtime = moment().add(livetime, 'minutes');
        endtime = endtime.format('YYYY-MM-DD H:mm:ss');

        //樣式
        var style = {
            fontfamily: $('#slt-fontfamily option:selected').eq(0).val(),
            color: $('#box-light').val(),
            size: $('#slt-size option:selected').eq(0).val(),
            face: Chatbox.face,
            animate: $('#slt-chatbox-ani option:selected').eq(0).val()
        };
        style = JSON.stringify(style);
        style = escape(style);

        //format
        var data = {
            text: text,
            endtime: endtime,
            style:style
        }
        query('add_chat', data, function () {
            History.update();
            Infowindow.loop();

        });
    }
}
var History = {
    init: function () {
        this.update();
        setInterval(this.update,5*1000);
    },
    format: function (text, style) {
        if (style == undefined || style == null) style = {};
        if (style.fontfamily == undefined) style.fontfamily = 'Microsoft JhengHei';
        if (style.size == undefined) style.size = '12';
        if (style.color == undefined) style.color = '#000';

        text = '<span style="font-family: ' + style.fontfamily + ';font-size:' + style.size + 'px;color:' + style.color + '">' + text + '</span>';


        return text;
    },
    update: function () {

        queryxml('get_chats', null, function (xml) {

            var xml = $.parseXML(xml);

            var chats = [];
            $(xml).find("Table").each(function () {

                var SN = $(this).find("SN").eq(0).text();
                var message = $(this).find("MESSAGE").eq(0).text();
                var time = $(this).find("TIME").eq(0).text();
                var name = $(this).find("name").eq(0).text();
                var lon = $(this).find("lon").eq(0).text();
                var lat = $(this).find("lat").eq(0).text();
                var ACCOUNT_SN = $(this).find("ACCOUNT_SN").eq(0).text();
                var EndInfowindowTime = $(this).find("EndInfowindowTime").eq(0).text();
                var style = $(this).find("STYLE").eq(0).text();

                style = unescape(style);
                style = $.parseJSON(style);
                time = time.replace('T', ' ');
                time = moment(time);
                EndInfowindowTime = EndInfowindowTime.replace('T', ' ');
                EndInfowindowTime = moment(EndInfowindowTime);

                SN = parseInt(SN);
                ACCOUNT_SN = parseInt(ACCOUNT_SN);

                lon = parseFloat(lon);
                lat = parseFloat(lat);

                message = History.format(message, style);

                var o = {
                    id: SN,
                    sn: SN,
                    message: message,
                    time: time,
                    endtime: EndInfowindowTime,
                    name: name,
                    position: { x: lon, lng: lon, y: lat, lat: lat },
                    account_sn: ACCOUNT_SN,
                    style: style
                }
                chats.push(o);
            });


            var li = $('#talk-template').html();
            var isAnyUpdated = false;
            chats.forEach(function (chat) {

                var existcount = $('#talk_box li[id=' + chat.sn + ']').length;
                if (existcount >= 1) return;

                var msg = li;
                msg = msg.replace('{sn}', chat.sn);
                msg = msg.replace('{content}', chat.message);
                msg = msg.replace('{name}', chat.name)
                msg = msg.replace('{picurl}', '../creation/homephoto/' + chat.account_sn +'.jpg');

                $('#talk_box').append(msg);
                isAnyUpdated = true;
            });

            //將scroll 最下方
            if (isAnyUpdated == true) {
                History.scrollBottom();
            }
            
        });
    },
    scrollBottom: function () {
        $('.talk_message').scrollTop($('#talk_box').height());
    }
}

var Panel = {
    init: function () {

    },
    goto: function (v) {
        if (v == '') return;
        v = parseInt(v);
        var position = [22.792250, 120.300137];
        switch (v) {
            case 0:
                position = [22.792250, 120.300137];//岡山
                break;
            case 1:
                position = [22.683614, 120.296530];//左營
                break;
            case 2:
                position = [22.611589, 120.300119];//前鎮
                break;
            case 3:
                position = [22.621442, 120.363136];//鳳山
                break;
            case 4:
                position = [22.884652, 120.482027];//旗山
                break;
            case 5:
                position = [23.154237, 120.640236];//桃源
                break;
            default:
                position = [Me.user.LAT, Me.user.LON];
                break;
        }
        var p = {}
        p.lat = position[0];
        p.lng = position[1];
        Googlemap.map.setCenter(p);
    },

    //////////// 遮蔽 //////////////
   
    mask: function (v) {
        //reset 
        Panel.maskinfowindow = false;
        Panel.maskhouse = false;
        Panel.maskanimation = false;
        Panel.mask_animation();
        Panel.mask_infowindow();
        Panel.mask_house();
        //
        var v = $('#slt-mask option:selected').val();
        switch (v) {
            case '0':
                Panel.maskinfowindow = true;
                this.mask_infowindow();
                break;
            case '1':
                Panel.maskanimation = true;
                this.mask_animation();
                break;
            case '2':
                Panel.maskhouse = true;
                this.mask_house();
                break;
            default:

                break;
        }
    },
    maskinfowindow: false,
    mask_infowindow: function () {
        
        if (Panel.maskinfowindow == true) {

            Infowindow.removeall();
        }
    },
    maskanimation:false,
    mask_animation: function () {

        if (Panel.maskanimation == true) {
            $('#map img[src*=animation]').hide();
        } else {
            $('#map img[src*=animation]').show();
        }
    },
    maskhouse: false,
    mask_house: function (sw) {

        if (Panel.maskhouse == true) {
            Infowindow.removeall();
            House.destoryall();
            $('#map img[src*=animation]').hide();
        } else {
            $('#map img[src*=animation]').show();
        }
    }
}
var House = {
    items: [],
    itemsAnimation: [],
    init: function (callback) {
        this.getData(callback);
        setInterval(this.getData, 5 * 1000);
        setInterval(this.styleing, 5*1000);
    },
    getData: function (callback) {

        query('get_all_user_online', null, function (usersd) {

            var users = $.parseJSON(usersd.d);
            users.forEach(function (user) {

                if (user.STYLE != '') {
                    style = unescape(user.STYLE);
                    style = $.parseJSON(style);
                }

                user.id = user.SN;
                user.sn = user.SN;
                user.style = style;
                user.creation = user.CREATION;
                user.name = user.NAME;
                user.address = user.ADDRESS;
                user.picture = user.PICTURE;
                user.lon = parseFloat(user.LON);
                user.lat = parseFloat(user.LAT);
                user.position = House.get_position(user);
            });
            House.update(users);

            if (callback != undefined) callback.call();
        });
    },
    /*檢查棲居的各項特效*/
    styleing: function () {

        for (var i = 0; i < House.items.length; i++) {
            var user = House.items[i];
            var style = user.style;

            var obj = House.itemsAnimation.getById(user.sn);


            if (style != undefined) {

                var athome = style.athome + '';
                //在不在家
                if (athome != undefined && athome != '' && athome != null) {
                    if (athome == '0') {
                        user.marker.setOpacity(0.55);
                    } else {
                        user.marker.setOpacity(1)
                    }
                }
            }


            /////////////////////////////////////////////////////
            // effect 
            //如果已經加入
            if (obj != null) {

                if (style.effect == undefined || style.effect != obj.style.effect) {
                    House.destory(House.itemsAnimation, user.sn, 'markerAnimation');

                    if (style.effect != '') {
                        user.iconurl = 'uploadfile/animation/' + style.effect + '.gif';
                        user.markerAnimation = House.build(user);
                        House.itemsAnimation.push(Object.assign({}, user));
                    }
                }

                continue;
            }

            //如果已經加入特效
            if (style != undefined) {

                var effect = style.effect;

                //有開啟特效
                if (effect != undefined && effect != '' && effect != null) {

                    user.iconurl = 'uploadfile/animation/' + effect + '.gif';
                    user.markerAnimation = House.build(user);
                    House.itemsAnimation.push(Object.assign({}, user));
                }


            }
        }
    },
    update: function (users) {

        //入境
        for (var i = 0; i < users.length; i++) {
            var user = users[i];
            var house = House.items.getById(user.sn);
            if (house == undefined) {
                this.immigrate(user);
            } else {
                for (var j = 0; j < House.items.length; j++) {
                    if (user.sn == House.items[j].sn)
                        House.items[j].style = user.style;
                }
            }
        }
        //出境
        for (var i = this.items.length - 1; i >= 0; i--) {
            var house = this.items[i];
            var user = users.getById(house.sn);
            if (user == undefined) {
                this.departure(house.sn);
            }
        }
        this.styleing();
    },
    immigrate: function (user) {

        if (user.creation != '') {
            user.iconurl = '../creation/homephoto/' + user.sn + '.jpg';
        } else {
            user.iconurl = '../creation/homephoto/account.png';
        }

        user.marker = House.build(user);
        House.items.push(user);
    },
    departure: function (sn) {
        House.destory(House.items, sn, 'marker');
        House.destory(House.itemsAnimation, sn, 'markerAnimation');
    },
    build: function (user) {
        //image
        var image = {
            url: user.iconurl,
            scaledSize: new google.maps.Size(80, 100),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(0, 0)
        };

        var position = user.position;
        var marker = new google.maps.Marker({
            position: position,
            icon: image,
            map: Googlemap.map,
            title: user.name,
            label: ' '
        });
        return marker;
    },
    get_position: function (user) {

        if (user.LON != null && user.LAT != null) {

            return {
                x: user.LON,
                y: user.LAT,
                lng: user.LON,
                lat: user.LAT
            };
        }
        var lon = 120.312172, lat = 22.623084;

        var city = user.address;
        switch (city) {
            case '楠梓區':
                lat = 22.717566;
                lon = 120.299934;
                break;
            case '左營區':
                lat = 22.683626;
                lon = 120.296697;
                break;
            case '鼓山區':
                lat = 22.651128;
                lon = 120.278629;
                break;
            case '三民區':
                lat = 22.648923;
                lon = 120.317346;
                break;
            case '鹽埕區':
                lat = 22.625134;
                lon = 120.283525;
                break;
            case '前金區':
                lat = 22.626369;
                lon = 120.293355;
                break;
            case '苓雅區':
            default:
                lat = 22.624303;
                lon = 120.327318;
                break;
        }
        lon = lon + (Math.random() / 100);
        return {
            x: lon,
            y: lat,
            lng: lon,
            lat: lat
        }
    },
    destory: function (items, sn, target) {
        for (var i = 0; i < items.length; i++) {

            if (items[i].sn == sn) {
                items[i][target].setMap(null);
                items.indexPop(i);
                return;
            }
        }
    },
    destoryall: function () {
        for (var i = House.items.length - 1; i >= 0; i--) {

            House.items[i].marker.setMap(null);
            if (House.items[i].itemsAnimation != null) {
                House.items[i].itemsAnimation.setMap(null);
            }
            House.items.indexPop(i);

        }
    }
}
var Infowindow = {
    items: [],
    init: function () {
        this.loop();
        setInterval(this.loop, 5 * 1000);
    },
    loop: function () {

        //remove expired
        for (var i = Infowindow.items.length - 1; i >= 0; i--) {
            var chat = Infowindow.items[i];

            if (moment() >= chat.endtime) {
                chat.infowindow.close();
                Infowindow.items.indexPop(i);
            }
        }

        //add
        queryxml('get_infowindows', null, function (xml) {

            var xml = $.parseXML(xml);

            var chats = [];
            $(xml).find("Table").each(function () {

                var SN = $(this).find("SN").eq(0).text();
                var message = $(this).find("MESSAGE").eq(0).text();
                var time = $(this).find("TIME").eq(0).text();
                var name = $(this).find("name").eq(0).text();
                var lon = $(this).find("lon").eq(0).text();
                var lat = $(this).find("lat").eq(0).text();
                var ACCOUNT_SN = $(this).find("ACCOUNT_SN").eq(0).text();
                var EndInfowindowTime = $(this).find("EndInfowindowTime").eq(0).text();
                var style = $(this).find("STYLE").eq(0).text();

                style = unescape(style);
                style = $.parseJSON(style);
                time = time.replace('T', ' ');
                time = moment(time);
                EndInfowindowTime = EndInfowindowTime.replace('T', ' ');
                EndInfowindowTime = moment(EndInfowindowTime);

                SN = parseInt(SN);
                ACCOUNT_SN = parseInt(ACCOUNT_SN);

                lon = parseFloat(lon);
                lat = parseFloat(lat);

                message = History.format(message, style);

                var o = {
                    id: SN,
                    sn: SN,
                    message: message,
                    time: time,
                    endtime: EndInfowindowTime,
                    name: name,
                    position: { x: lon, lng: lon, y: lat, lat: lat },
                    account_sn: ACCOUNT_SN,
                    style: style
                }
                Infowindow.add(o);
            });
            
        });

    },
    update: function (chat) {
        var infow = Infowindow.items.getById(chat.account_sn);
        Infowindow.formatAnimate(chat);
        infow.infowindow.setContent(Infowindow.getContent(chat))

        if (chat.style == undefined) chat.style = { face: 0 };
        if (chat.style.face == undefined) chat.style.face = 0
        $('.iw-face-' + chat.account_sn).attr('src', 'uploadfile/face/' + chat.style.face + '.png');
    },
    add: function (chat) {
        //如果遮蔽的話
        if (Panel.maskinfowindow == true) return;
        //過時的話
        if (moment() >= chat.endtime) {
            return;
        }
        //如果已經加過了
        if (Infowindow.items.getById(chat.account_sn) != undefined) {
            this.update(chat);
            return;
        } 

        //start adding

        Infowindow.formatAnimate(chat);
        var content = Infowindow.getContent(chat);
        var infowindow = new google.maps.InfoWindow({
            content: content,
            maxWidth: 350
        });

        Infowindow.items.push({
            id: chat.account_sn,
            account_sn: chat.account_sn,
            infowindow: infowindow,
            endtime: chat.endtime
        });


        var m = House.items.getById(chat.account_sn);
        if (m != undefined) {
            infowindow.open(Googlemap.map, m.marker);
            setTimeout(function () {
                var id = '#iw-container-' + chat.account_sn;
                var faceicon = $('#ul-face li.select-face').val();
                if (faceicon == undefined) {
                    faceicon = '0';
                }
                faceicon = 'uploadfile/face/' + faceicon + '.png';
                var cls = 'iw-face-' + chat.account_sn;
                $('<img class="' + cls + '" src="' + faceicon + '" width="40" height="40" style="cursor: default; position: absolute;   left: -20px; top: -20px; z-index: 7;"/>').insertBefore($(id).parent().parent().parent().parent()[0].childNodes[0]);
                $(id).parent().parent().parent().next().hide()

                $(id).parent().parent().parent().prev()[0].childNodes[3].style.borderRadius = '12px'

                $(id).parent().parent().parent().prev()[0].childNodes[1].style.borderRadius = '12px'
            }, 100);

        }

    },
    removeall: function () {
        for (var i = Infowindow.items.length - 1; i >= 0; i--) {
            var chat = Infowindow.items[i];
            chat.infowindow.close();
            Infowindow.items.indexPop(i);
        }
    },
    getContent: function (chat) {
        var content = '<div class="infowindow" id="iw-container-' + chat.account_sn + '">' +
            chat.message +
            '</div>';
        return content;
    },
    formatAnimate: function (chat) {
        if (chat.style.animate != undefined) {
            switch (chat.style.animate.toLowerCase()) {
                case 'marquee':
                    chat.message = '<marquee direction="right" width="160" class="hh">' + chat.message + '</marquee>';
                    break;
                case 'typing':
                    chat.message = '<div class="typing">' + chat.message + '</div>';
                    break;
            }
        }

    }
}
var Online = {
    init: function () {
        this.enter();

        setInterval(this.update, 1000 * 30);

        window.onunload = this.leave;
    },
    enter: function () {
        query('enter');
    },
    leave: function () {
        query('leave');
    },
    update: function () {
        query('update');
    }
}

var Spot = {
    map: null,
    init: function (map) {
        this.map = map;
        this.load_all();
    },
    load_all: function () {

        var ajax = $.ajax({
            url: "services.aspx/get_spots",
            type: "POST",
            dataType: 'text',
            contentType: "application/json; charset=utf-8",
            timeout: 10 * 1000,
            success: function (xml) {

                var xml = $.parseXML(xml);

                $(xml).find("Table").each(function () {

                    var sn = $(this).find("SN").eq(0).text();
                    var lon = $(this).find("LON").eq(0).text();
                    var lat = $(this).find("LAT").eq(0).text();
                    var filename = $(this).find("FILENAME").eq(0).text();
                    var width = $(this).find("WIDTH").eq(0).text();
                    var height = $(this).find("HEIGHT").eq(0).text();
                    var x = parseFloat(lon);
                    var y = parseFloat(lat);

                    if (width == '') { width = 60; } else { width = parseInt(width); }
                    if (height == '') { height = 60; } else { height = parseInt(height); }

                    Spot.add_spot_to_map(sn, filename, x, y);

                });
                
            },
            error: null
        });
    },
    add_spot_to_map: function (sn, filename, x, y) {

        var image = {
            url: '../manage/com/' + filename,
            scaledSize: new google.maps.Size(60, 60), // scaled size
            origin: new google.maps.Point(0, 0), // origin
            anchor: new google.maps.Point(0, 0) // anchor
        };

        var marker = new google.maps.Marker({
            id: sn,
            sn: sn,
            map: this.map,
            title: "",
            icon: image,
            draggable: false,
            position: new google.maps.LatLng(y, x),
            opacity: 0.6,
            zIndex: 1
        });

    }
}
function query(s, data, success, fail) {

    var datastring = "";
    if (data != null) {
        for (var key in data) {
            datastring += key + ":\"" + data[key] + "\",";
        }
        datastring = datastring.substring(0, datastring.length - 1);
        datastring = "{" + datastring + "}";
    }


    var ajax = $.ajax({
        url: "services.aspx/" + s,
        type: "POST",
        data: datastring,
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        timeout: 10 * 1000,
        success: success,
        error: fail
    });
}
function queryxml(s, data, success, fail) {

    var datastring = "";
    if (data != null) {
        for (var key in data) {
            datastring += key + ":\"" + data[key] + "\",";
        }
        datastring = datastring.substring(0, datastring.length - 1);
        datastring = "{" + datastring + "}";
    }


    var ajax = $.ajax({
        url: "services.aspx/" + s,
        type: "POST",
        data: datastring,
        dataType: 'text',
        contentType: "application/json; charset=utf-8",
        timeout: 10 * 1000,
        success: success,
        error: fail
    });
}
