
	$(".icon_talkboxclose").click(function () {
		$(".talk_area, .icon_talkboxclose, .icon_talkboxopen ").fadeToggle(480);
	});
	$(".icon_talkboxopen").click(function () {
		$(".talk_area, .icon_talkboxclose, .icon_talkboxopen ").fadeToggle(480);
	});


    //talk_set
	function elHide(talkSetBox,talkSetIcon){ 

		$(".box_status, .box_sticker, .box_set").hide();
		$(".box_status, .box_sticker, .box_set").removeClass("fadeinLeft");
		$(".icon_status, .icon_sticker, .icon_set").removeClass("active");

		$(talkSetBox).toggle();
		$(talkSetBox).toggleClass("fadeinLeft");
		$(talkSetIcon).toggleClass("active");
	}

	$(".icon_set").click(function () {
		elHide(".box_set",".icon_set"); 
	});

	$(".icon_status").click(function () {
		elHide(".box_status",".icon_status");
	});

	$(".icon_sticker").click(function () {
		elHide(".box_sticker",".icon_sticker");
	});

	$(".icon_close").click(function () {
		$(".box_set, .box_status, .box_sticker").hide();
		$(".icon_set, .icon_status, .icon_sticker").removeClass("active");
	});


	//tab
	$(function(){
		var $li = $('ul.tab-title li');
		$($li. eq(0) .addClass('active').find('a').attr('href')).siblings('.tab-inner').hide();

		$li.click(function(){
			$($(this).find('a'). attr ('href')).show().siblings ('.tab-inner').hide();
			$(this).addClass('active'). siblings ('.active').removeClass('active');
		});
	});


	//menu
	$(".menu").click(function () {
		$(".menu_set").fadeToggle(360);
	});

