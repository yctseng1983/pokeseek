﻿
function query(s,data,success,fail) {

    var datastring = "";
    for (var key in data) {
        datastring += key + ":\"" + data[key] + "\",";
    }
    datastring = datastring.substring(0, datastring.length - 1);
    datastring = "{" + datastring + "}";

    var ajax = $.ajax({
        url: "services.aspx/"+s,
        type: "POST",
        data: datastring,
        dataType: 'text',
        contentType: "application/json; charset=utf-8",
        timeout: 10 * 1000,
        success: success,
        error: fail
    });
}
function queryP(s, data, success, fail) {

    var datastring = "";
    for (var key in data) {
        datastring += key + ":\"" + data[key] + "\",";
    }
    datastring = datastring.substring(0, datastring.length - 1);
    datastring = "{" + datastring + "}";

    var ajax = $.ajax({
        url: "../../s.aspx/" + s,
        type: "POST",
        data: datastring,
        dataType: 'text',
        contentType: "application/json; charset=utf-8",
        timeout: 10 * 1000,
        success: success,
        error: fail
    });
}