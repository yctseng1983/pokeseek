<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.8.2</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <true/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>json</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../imgs/game.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../../阿信/海洋遊戲_game1/03_game1_plat.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,54,63,107</rect>
                <key>scale9Paddings</key>
                <rect>32,54,63,107</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../阿信/海洋遊戲_game1/04_detection_L_1.png</key>
            <key type="filename">../../../../阿信/海洋遊戲_game1/04_detection_L_2.png</key>
            <key type="filename">../../../../阿信/海洋遊戲_game1/04_detection_L_3.png</key>
            <key type="filename">../../../../阿信/海洋遊戲_game1/04_detection_R_1.png</key>
            <key type="filename">../../../../阿信/海洋遊戲_game1/04_detection_R_2.png</key>
            <key type="filename">../../../../阿信/海洋遊戲_game1/04_detection_R_3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>36,60,72,119</rect>
                <key>scale9Paddings</key>
                <rect>36,60,72,119</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../阿信/海洋遊戲_game1/04_game1_btn2.png</key>
            <key type="filename">../../../../阿信/海洋遊戲_game1/04_game1_btn3.png</key>
            <key type="filename">../../../../阿信/海洋遊戲_game1/04_game1_btn4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>23,18,47,35</rect>
                <key>scale9Paddings</key>
                <rect>23,18,47,35</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../阿信/海洋遊戲_game1/04_game1_shop_object1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>157,112,313,225</rect>
                <key>scale9Paddings</key>
                <rect>157,112,313,225</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../阿信/海洋遊戲_game1/04_game1_shop_object2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>137,140,274,280</rect>
                <key>scale9Paddings</key>
                <rect>137,140,274,280</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../阿信/海洋遊戲_game1/04_game1_shop_object3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>152,115,304,229</rect>
                <key>scale9Paddings</key>
                <rect>152,115,304,229</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../阿信/海洋遊戲_game1/04_game1_shop_object4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>80,132,160,263</rect>
                <key>scale9Paddings</key>
                <rect>80,132,160,263</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../阿信/海洋遊戲_game1/06_battery1.png</key>
            <key type="filename">../../../../阿信/海洋遊戲_game1/06_battery2.png</key>
            <key type="filename">../../../../阿信/海洋遊戲_game1/06_battery3.png</key>
            <key type="filename">../../../../阿信/海洋遊戲_game1/06_battery4.png</key>
            <key type="filename">../../../../阿信/海洋遊戲_game1/06_battery5.png</key>
            <key type="filename">../../../../阿信/海洋遊戲_game1/06_battery6.png</key>
            <key type="filename">../../../../阿信/海洋遊戲_game1/06_battery7.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>48,25,95,50</rect>
                <key>scale9Paddings</key>
                <rect>48,25,95,50</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../阿信/海洋遊戲_game1/06_money.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>71,14,143,28</rect>
                <key>scale9Paddings</key>
                <rect>71,14,143,28</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../../阿信/海洋遊戲_game1/06_battery4.png</filename>
            <filename>../../../../阿信/海洋遊戲_game1/06_battery5.png</filename>
            <filename>../../../../阿信/海洋遊戲_game1/06_battery6.png</filename>
            <filename>../../../../阿信/海洋遊戲_game1/06_battery7.png</filename>
            <filename>../../../../阿信/海洋遊戲_game1/06_money.png</filename>
            <filename>../../../../阿信/海洋遊戲_game1/03_game1_plat.png</filename>
            <filename>../../../../阿信/海洋遊戲_game1/04_detection_L_1.png</filename>
            <filename>../../../../阿信/海洋遊戲_game1/04_detection_L_2.png</filename>
            <filename>../../../../阿信/海洋遊戲_game1/04_detection_L_3.png</filename>
            <filename>../../../../阿信/海洋遊戲_game1/04_detection_R_1.png</filename>
            <filename>../../../../阿信/海洋遊戲_game1/04_detection_R_2.png</filename>
            <filename>../../../../阿信/海洋遊戲_game1/04_detection_R_3.png</filename>
            <filename>../../../../阿信/海洋遊戲_game1/04_game1_btn2.png</filename>
            <filename>../../../../阿信/海洋遊戲_game1/04_game1_btn3.png</filename>
            <filename>../../../../阿信/海洋遊戲_game1/04_game1_btn4.png</filename>
            <filename>../../../../阿信/海洋遊戲_game1/04_game1_shop_object1.png</filename>
            <filename>../../../../阿信/海洋遊戲_game1/04_game1_shop_object2.png</filename>
            <filename>../../../../阿信/海洋遊戲_game1/04_game1_shop_object3.png</filename>
            <filename>../../../../阿信/海洋遊戲_game1/04_game1_shop_object4.png</filename>
            <filename>../../../../阿信/海洋遊戲_game1/06_battery1.png</filename>
            <filename>../../../../阿信/海洋遊戲_game1/06_battery2.png</filename>
            <filename>../../../../阿信/海洋遊戲_game1/06_battery3.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
