﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Villages_Pokeseek_data : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }



    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
    public static string add_fish(string page,string fishtype)
    {
        Common.MSSQL_M db = new Common.MSSQL_M();
        ArrayList pars = new ArrayList();
        string account_sn = Global.get_account_sn();

        if (account_sn == "") return "0";
        string sql = @"
            INSERT INTO [dbo].[FISHING]
                       ([ACCOUNT_SN]
                       ,[PAGE]
                       ,[FISHTYPE])
                 VALUES
                       (@ACCOUNT_SN
                       ,@PAGE
                       ,@FISHTYPE)
        ";
        pars.Add(new SqlParameter("@ACCOUNT_SN", account_sn));
        pars.Add(new SqlParameter("@PAGE", page));
        pars.Add(new SqlParameter("@FISHTYPE", fishtype));
        int r = db.Execute(sql, pars);
        if (r >= 1)
        {
            return r.ToString();
        }
        return "0";
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
    public static string query_wallet()
    {
        Common.MSSQL_M db = new Common.MSSQL_M();
        ArrayList pars = new ArrayList();
        string account_sn = Global.get_account_sn();

        if (account_sn == "") return "0";
        string sql = @"
                SELECT SUM(money) m
                  FROM [WALLET]
                  WHERE ACCOUNT_SN=@ACCOUNT_SN 
        ";
        pars.Add(new SqlParameter("@ACCOUNT_SN", account_sn));
        int r = db.ExecuteScalar(sql, pars);

        return r.ToString();
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Xml)]    
    public static DataTable get_fishtype(string page)
    {
        Common.MSSQL_M db = new Common.MSSQL_M();
        ArrayList pars = new ArrayList();
        string account_sn = Global.get_account_sn();

        if (account_sn == "") return new DataTable();
        string sql = @"
                SELECT COUNT(*) CNT,FISHTYPE
                  FROM [FISHING] 
                  WHERE ACCOUNT_SN=@ACCOUNT_SN
                  AND PAGE=@PAGE
                    GROUP BY ACCOUNT_SN,FISHTYPE
        ";
        pars.Add(new SqlParameter("@ACCOUNT_SN", account_sn));
        pars.Add(new SqlParameter("@PAGE", page));
        DataTable dt = db.Select(sql, pars);

        return dt;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
    public static string earn(string type, string money)
    {
        Common.MSSQL_M db = new Common.MSSQL_M();
        ArrayList pars = new ArrayList();
        string account_sn = Global.get_account_sn();

        if (account_sn == "") return "0";

        //add walet
        string sql = @"
            INSERT INTO [dbo].[WALLET]
                       ([MONEY]
                       ,[ACCOUNT_SN]
                       ,[TYPE])
                 VALUES
                       (@MONEY
                       ,@ACCOUNT_SN
                       ,@TYPE)        
        ";
        pars.Add(new SqlParameter("@ACCOUNT_SN", account_sn));
        pars.Add(new SqlParameter("@TYPE", type));
        pars.Add(new SqlParameter("@MONEY", money));
        int r = db.Execute(sql, pars);
        return r.ToString();
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
    public static string sale_fish(string count,string type,string money)
    {
        Common.MSSQL_M db = new Common.MSSQL_M();
        ArrayList pars = new ArrayList();
        string account_sn = Global.get_account_sn();

        if (account_sn == "") return "0";
        
        //add walet
        earn(type, money);
        //delete book fish
        string sql = @"
                DELETE TOP (@COUNT) FROM [dbo].[FISHING]
                      WHERE FISHTYPE=@FISHTYPE
                            and ACCOUNT_SN=@ACCOUNT_SN
        ";
        pars.Clear();
        sql = sql.Replace("@COUNT", count);
        pars.Add(new SqlParameter("@ACCOUNT_SN", account_sn));
        pars.Add(new SqlParameter("@FISHTYPE", type));
        int r = db.Execute(sql, pars);
        if (r <= 0) return "0";
        return r.ToString();
    }
}