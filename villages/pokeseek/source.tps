<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.8.1</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <true/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>json</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>4096</int>
            <key>height</key>
            <int>4096</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../Pokefound/source.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">imgs/01_bg.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>300,179,600,357</rect>
                <key>scale9Paddings</key>
                <rect>300,179,600,357</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">imgs/05_menu_1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>23,12,46,23</rect>
                <key>scale9Paddings</key>
                <rect>23,12,46,23</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">imgs/05_menu_2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,17,63,33</rect>
                <key>scale9Paddings</key>
                <rect>32,17,63,33</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">imgs/05_menu_3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>27,11,53,22</rect>
                <key>scale9Paddings</key>
                <rect>27,11,53,22</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">imgs/08_detection_L_1.png</key>
            <key type="filename">imgs/08_detection_L_2.png</key>
            <key type="filename">imgs/08_detection_L_3.png</key>
            <key type="filename">imgs/08_detection_R_1.png</key>
            <key type="filename">imgs/08_detection_R_2.png</key>
            <key type="filename">imgs/08_detection_R_3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>84,92,167,184</rect>
                <key>scale9Paddings</key>
                <rect>84,92,167,184</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../阿信/海底遊戲前-0629-1/06_wave.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>371,32,741,63</rect>
                <key>scale9Paddings</key>
                <rect>371,32,741,63</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../阿信/海底遊戲前-0629-1/06_wave_2.jpg</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>371,926,741,1853</rect>
                <key>scale9Paddings</key>
                <rect>371,926,741,1853</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../阿信/海底遊戲前/02_wave.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>363,29,727,57</rect>
                <key>scale9Paddings</key>
                <rect>363,29,727,57</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../阿信/海底遊戲前/03_ship.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>207,94,415,189</rect>
                <key>scale9Paddings</key>
                <rect>207,94,415,189</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../阿信/海底遊戲前/04_wave.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>375,39,750,77</rect>
                <key>scale9Paddings</key>
                <rect>375,39,750,77</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../阿信/海底遊戲前/05_menu.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>34,55,67,111</rect>
                <key>scale9Paddings</key>
                <rect>34,55,67,111</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../阿信/海底遊戲前/07_explanation.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>145,57,289,114</rect>
                <key>scale9Paddings</key>
                <rect>145,57,289,114</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../阿信/海底遊戲前/09_seaweed.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>282,120,563,240</rect>
                <key>scale9Paddings</key>
                <rect>282,120,563,240</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../阿信/海底遊戲前/10_seabed.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>282,90,563,179</rect>
                <key>scale9Paddings</key>
                <rect>282,90,563,179</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../阿信/海底遊戲前/11_skip.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>25,13,49,25</rect>
                <key>scale9Paddings</key>
                <rect>25,13,49,25</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../阿信/海底遊戲前/12_fish01.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>39,20,77,39</rect>
                <key>scale9Paddings</key>
                <rect>39,20,77,39</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../阿信/海底遊戲前/12_fish02.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>43,37,87,74</rect>
                <key>scale9Paddings</key>
                <rect>43,37,87,74</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../阿信/海底遊戲前/12_fish03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>64,33,129,65</rect>
                <key>scale9Paddings</key>
                <rect>64,33,129,65</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../阿信/海底遊戲前/12_fish04.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>40,16,79,33</rect>
                <key>scale9Paddings</key>
                <rect>40,16,79,33</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../阿信/海底遊戲前/12_trash01.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>24,24,47,47</rect>
                <key>scale9Paddings</key>
                <rect>24,24,47,47</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../阿信/海底遊戲前/12_trash02.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>37,35,73,69</rect>
                <key>scale9Paddings</key>
                <rect>37,35,73,69</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../阿信/海底遊戲前/12_trash03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>37,48,75,97</rect>
                <key>scale9Paddings</key>
                <rect>37,48,75,97</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../阿信/海底遊戲前/12_trash04.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>45,45,89,89</rect>
                <key>scale9Paddings</key>
                <rect>45,45,89,89</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../阿信/海底遊戲前/13_clound_b03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,67,31,134</rect>
                <key>scale9Paddings</key>
                <rect>16,67,31,134</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../阿信/海底遊戲前/13_clound_w01.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>26,83,52,167</rect>
                <key>scale9Paddings</key>
                <rect>26,83,52,167</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../阿信/海底遊戲前/12_fish04.png</filename>
            <filename>../../../阿信/海底遊戲前/11_skip.png</filename>
            <filename>../../../阿信/海底遊戲前/13_clound_w01.png</filename>
            <filename>../../../阿信/海底遊戲前/13_clound_b03.png</filename>
            <filename>../../../阿信/海底遊戲前/04_wave.png</filename>
            <filename>../../../阿信/海底遊戲前/10_seabed.png</filename>
            <filename>../../../阿信/海底遊戲前/02_wave.png</filename>
            <filename>../../../阿信/海底遊戲前/09_seaweed.png</filename>
            <filename>../../../阿信/海底遊戲前/03_ship.png</filename>
            <filename>../../../阿信/海底遊戲前/07_explanation.png</filename>
            <filename>../../../阿信/海底遊戲前/12_trash04.png</filename>
            <filename>../../../阿信/海底遊戲前/12_fish03.png</filename>
            <filename>../../../阿信/海底遊戲前/12_trash03.png</filename>
            <filename>../../../阿信/海底遊戲前/05_menu.png</filename>
            <filename>../../../阿信/海底遊戲前/12_trash02.png</filename>
            <filename>../../../阿信/海底遊戲前/12_fish02.png</filename>
            <filename>../../../阿信/海底遊戲前/12_fish01.png</filename>
            <filename>../../../阿信/海底遊戲前/12_trash01.png</filename>
            <filename>imgs/01_bg.png</filename>
            <filename>../../../阿信/海底遊戲前-0629-1/06_wave_2.jpg</filename>
            <filename>../../../阿信/海底遊戲前-0629-1/06_wave.png</filename>
            <filename>imgs/08_detection_R_2.png</filename>
            <filename>imgs/08_detection_R_3.png</filename>
            <filename>imgs/05_menu_1.png</filename>
            <filename>imgs/05_menu_2.png</filename>
            <filename>imgs/05_menu_3.png</filename>
            <filename>imgs/08_detection_L_1.png</filename>
            <filename>imgs/08_detection_L_2.png</filename>
            <filename>imgs/08_detection_L_3.png</filename>
            <filename>imgs/08_detection_R_1.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
