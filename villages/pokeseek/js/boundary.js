﻿var Steps = {
    index: 0,
    items: [[0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0]],
    record: function (rov) {
        
        Steps.items[Steps.index][0] = rov.x;
        Steps.items[Steps.index][1] = rov.y;
        Steps.index++;
        if (Steps.index >= Steps.items.length) {
            Steps.index = 0;
        } 
    },
    backto: function (room,rov) {
        try {
            var i = Steps.index - 10;
            
            if (i < 0) {
                i = Steps.items.length - 1 + i;
            }
            var step = Steps.items[i];
            var stepm;
            if (i - 1 < 0) {
                stepm = Steps.items[Steps.items.length-1];
            } else {
                stepm = Steps.items[i - 1];
            }
            rov.x += step[0] - stepm[0];
            rov.y += step[1] - stepm[1];
            //room.x = step[0];
            //room.y = step[1];
        } catch (err) {
            console.log(err);
        }
        
    }
}
var Boundary = {
    _room: null,
    _polygons: [],
    _bump: null,
    _historySteps:[],//紀錄軌跡
    init: function (room) {
        this._room = room;

        // start draw
        var rps = [];
        var ppoints = [];
        for (var i = 0; i < __boundary.length; i++) {
            var boundarys = __boundary[i];

            var offset = __offset;
            var points = boundarys.points;
            var oj = __offsetJudge;
            rps = [];
            ppoints = [];
            for (var j = 0; j < points.length; j++) {
                var point = points[j];
                var x = point.x + offset.x;
                var y = point.y + offset.y;
                rps.push(x);
                rps.push(y);
                ppoints.push({ x: point.x + oj.x, y: point.y + oj.y });

            }
            this.draw(rps, offset, ppoints);
        }
    },
    in: function (room, rov, dir) {

        
        //
        var x = Math.abs(room.x) + rov.x;
        var y = Math.abs(room.y) + rov.y;

        var plt = { x: x + (rov.width * 0.7), y: y + (rov.height * 0.75) };
        var prt = { x: x + (rov.width * 0.75), y: y + (rov.height * 0.75) };
        var plb = { x: x + (rov.width * 0.7), y: y + (rov.height * 0.8) };
        var prb = { x: x + (rov.width * 0.75), y: y + + (rov.height * 0.8) };

        if (dir == 'A') {
            plt.x += __dir.A;
            plb.x += __dir.A;
        }


        //console.log('plt' + plt.x + "_" + plt.y);
        //console.log('prt' + prt.x + "_" + plt.y);
        var r1 = false, r2 = false, r3 = false, r4 = false, r = false;
        for (var i = 0; i < this._polygons.length; i++) {
            var polygon = this._polygons[i];

            r1 = MM.GIS.isInsidePolygon(polygon.points, { x: plt.x, y: plt.y });
            r2 = MM.GIS.isInsidePolygon(polygon.points, { x: prt.x, y: prt.y });
            r3 = MM.GIS.isInsidePolygon(polygon.points, { x: plb.x, y: plb.y });
            r4 = MM.GIS.isInsidePolygon(polygon.points, { x: prb.x, y: prb.y });


            if (dir == 'A') {
                r = r1 || r3;
            }else if (dir == 'D') {
                r = r2 || r4;
            }else if (dir == 'W') {
                r = r1 || r2;
            }else if(dir == 'X') {
                r = r3 || r4;
            } else{
                r = r1 || r2 || r3 || r4; 
            }

            if (r) {
                //console.log(true);

                switch (dir) {
                    case 'A':
                       // rov.x += 1;
                        break;
                    case 'D':
                       // rov.x -= 1;
                        break;
                    case 'X':
                        //rov.y -= 1;
                        break;
                    case 'W':
                        //rov.y += 1;
                        break;
                }

                //回到之權OK的位置
                if (Rov != undefined && Rov.rise == true) {

                } else {
                   
                }
                Steps.backto(room, rov);
                return true;
            }
        }
        //console.log(false);
        //紀錄軌跡
        Steps.record(room);
        return false;
    },
    draw: function (rpt, offset, points) {

        var triangle = new Graphics();
        triangle.beginFill(0x66FF33);


        triangle.drawPolygon(rpt);

        //Fill shape's color
        triangle.endFill();

        triangle.x = offset.x;
        triangle.y = offset.y;
        triangle.points = points;
        triangle.alpha = 0;

        this._polygons.push(triangle);
        this._room.addChild(triangle);
    }
}