﻿
var shallowName = '淺水區';
var deepName = '深海區';
var mazeName = '迷宮區';
var freakName = '科幻區';
var ryukyuName = '琉球區';

if (!Boundary) var Boundary = null;
var Keyboard = {
    direction: { W: false, A: false, X: false, D: false },
    init: function () {
        var W = keyboard(87);
        var A = keyboard(65);
        var X = keyboard(83);//s
        var D = keyboard(68);
        var E = keyboard(69);//book
        var UP = keyboard(38);
        var DOWN = keyboard(40);
        var LEFT = keyboard(37);
        var RIGHT = keyboard(39);
        var space = keyboard(32);
        space.press = function () {
            Rov.runCatch();
        };
        space.release = function () {

        };
        A.press = function () { Keyboard.direction.A = true; }
        A.release = function () { Keyboard.direction.A = false; }
        D.press = function () { Keyboard.direction.D = true; }
        D.release = function () { Keyboard.direction.D = false; }
        X.press = function () {
                        
            if (Rov.rise == undefined) {
                Keyboard.direction.X = true;
            } else {
                if (Rov.rise == false) {
                    Keyboard.direction.X = true;
                }
            }
            
        }
        X.release = function () { Keyboard.direction.X = false; }
        W.press = function () { Keyboard.direction.W = true; }
        W.release = function () { Keyboard.direction.W = false; }
        /*
        UP.press = function () {
            Rov.takePhoto();

            setTimeout(function () {
                $('#camera').show();
                setTimeout(function () {
                    $('#camera').fadeOut();
                }, 800);
            }, 200);
        }
        DOWN.press = UP.press;
        LEFT.press = UP.press;
        RIGHT.press = UP.press;
        */
    },
    roomControl: function (delta) {
        //移動room
        if (this.direction.W == true) {
            if (Boundary != null && Boundary.in(groupRoom, Rov.group, 'W') == true) {
                return;
            } else {
                //groupRoom.y += groupRoom.vy;

            }
            if (Rov.group.y < __upLimit) {            
                groupRoom.y += groupRoom.vy;
            } else {
                Rov.group.y -= Rov.group.vy;
            }

            Depth.update();
        }
        if (this.direction.X == true) {
            if (Boundary != null && Boundary.in(groupRoom, Rov.group, 'X') == true) {
                return;
            } else {
                //groupRoom.y -= groupRoom.vy;

            }
            if (Rov.group.y >= Config.stage.h - __downLimit) {            
                groupRoom.y -= groupRoom.vy;
            } else {
                Rov.group.y += Rov.group.vy;
            }

            Depth.update();
        }
        if (this.direction.A == true) {
            if (Boundary != null && Boundary.in(groupRoom, Rov.group, 'A') == true) {
                return;
            }

            if (Rov.group.x < 400) {
                //Rov.group.x -= Rov.group.vx;
                groupRoom.x += groupRoom.vx;
            } else {
                //groupRoom.x += groupRoom.vx;
                Rov.group.x -= Rov.group.vx;
            }

            Rov.left();
        }
        if (this.direction.D == true) {

            if (Boundary != null && Boundary.in(groupRoom, Rov.group, 'D') == true) {
                return;
            }

            if (Rov.group.x >= Config.stage.w - 400) {
                //Rov.group.x += Rov.group.vx;
                groupRoom.x -= groupRoom.vx;
            } else {
                Rov.group.x += Rov.group.vx;
                //groupRoom.x -= groupRoom.vx;
                //Rov.group.x += Rov.group.vx;
            }

            Rov.right();
        }

        //框住room不超出去

        //go up
        if (groupRoom.y >= 0) {
            groupRoom.y = 0;
            //rov
            if (Rov.group.y >= 10 && this.direction.W == true) {
                Rov.group.y -= Rov.group.vy;
                //                
                if (groupRoom.y == 0) {
                    if (DepthRov != null && Boundary != null && Boundary.in(groupRoom, Rov.group, 'W') == false) {
                        DepthRov -= 2;                  
                    }
                    
                }
            }
        }

        //go down
        if (groupRoom.y <= -(groupRoom.height - Config.stage.h - __downBoundary)) {
            groupRoom.y = -(groupRoom.height - Config.stage.h - __downBoundary);
            //rov            
            if (this.direction.X == true) {
                Rov.group.y += Rov.group.vy;
                //                
                if (Rov.group.y >= groupRoom.y) {                    
                    if (Rov.group.y <= (Config.stage.h - Rov.group.height - 30)) {
                        if (DepthRov != null && Boundary != null && Boundary.in(groupRoom, Rov.group, 'X') == false) {
                            DepthRov += 2;                  
                        }                 
                    }
                }
            }
            if ((Rov.group.y + Rov.group.height) >= Config.stage.h - 30 && this.direction.X == true) {
                Rov.group.y = Config.stage.h - Rov.group.height - 30;                
            }
        }

        //go right
        //let rightBoundary = (Config.room.w * -1) + Config.stage.w -100;
        let rightBoundary = __rightBoundary;
        if (groupRoom.x <= rightBoundary) {
            groupRoom.x = rightBoundary;
            //rov
            if (this.direction.D == true) {
                Rov.group.x += Rov.group.vx;
            }
            if ((Rov.group.x + Rov.group.width) >= Config.stage.w - 50 && this.direction.D == true) {

                Rov.group.x = Config.stage.w - Rov.group.width - 50;
            }
        }
        //go left
        if (groupRoom.x >= 0) {
            groupRoom.x = 0;
            //rov
            if (Rov.group.x >= 10 && this.direction.A == true) {
                Rov.group.x -= Rov.group.vx;
            }
        }

    }
}
var Book = {
    group: null,
    groupPage: null,
    pageIndex: 0, // from 0~~n corresponse to fishtype
    spritePages: [],
    pool: [],
    button: { sale: null },
    spries: {
        sea: null,
        seaFreak: null,
        seaWater: null,
        seaDeep: null,        
        seaRyukyu: null,
    },    
    saleCount: 0,
    LVs: [],
    isOnline:false,
    accountName: null,
    seaWidthKeep: 0,
    seaHeightKeep: 0,

    /* ************************************************ */
    //跟 freak.html 要一致   
    _FishTypeFreak: [
        { id:  0, price:  0, count: 0 },//巨齒鯊
        { id:  1, price:520, count: 0 },//盲魚客
        { id:  2, price:599, count: 0 },//紅牛
        { id:  3, price:369, count: 0 },//怪眼豚
        { id:  4, price:168, count: 0 },//刺山龜
        { id:  5, price:399, count: 0 },//小蝸
        { id:  6, price:159, count: 0 },//鳥羽魚
        { id:  7, price:469, count: 0 },//獨角豚
        { id:  8, price:288, count: 0 },//雙頭魚
        { id:  9, price:419, count: 0 },//三眼比目
        { id: 10, price: 79, count: 0 },//藍精靈
        { id: 11, price: 99, count: 0 },//金線魚
        { id: 12, price: 38, count: 0 }],//剪刀蝦        
    _FishItemsFreak: [], 
    //跟 shallow.html 要一致
    _FishTypeShallow: [
        { id: 0, price: 50 ,count: 0 },
        { id: 1, price: 55, count: 0 },
        { id: 2, price: 85, count: 0 },
        { id: 3, price: 70, count: 0 },
        { id: 4, price: 95, count: 0 },
        { id: 5, price: 50, count: 0 },
        { id: 6, price: 70, count: 0 }],
    _FishItemsShallow: [],    
    //跟 deep.html 要一致
    _FishTypeDeep: [
        { id: 0, price: 999, count: 0 },
        { id: 1, price: 599, count: 0 },
        { id: 2, price: 399, count: 0 },
        { id: 3, price: 799, count: 0 },
        { id: 4, price: 99, count: 0 },
        { id: 5, price: 299, count: 0 },
        { id: 6, price: 199, count: 0 },
        { id: 7, price: 799, count: 0 },
        { id: 8, price: 99, count: 0 },
        { id: 9, price: 199, count: 0 }],
    _FishItemsDeep: [],
    //跟 ryukyu.html 要一致
    _FishTypeRyukyu: [
        { id: 0, price:650, count: 0 },
        { id: 1, price:230, count: 0 },
        { id: 2, price:350, count: 0 },
        { id: 3, price:230, count: 0 },
        { id: 4, price:350, count: 0 },
        { id: 5, price:380, count: 0 },
        { id: 6, price:250, count: 0 },
        { id: 7, price:500, count: 0 }],
    _FishItemsRyukyu: [],
    //
    _FishType: [],
    _FishItems: [],
    /* ************************************************ */

    init: function () {

        $.ajax({
            url: "../../s.aspx/isonline",
            type: "POST",
            dataType: 'text',
            timeout:60*1000,
            contentType: "application/json; charset=utf-8",
            success: function (xml) {
                if (xml == '') {
                   
                } else {
                    Book.isOnline = true;
                }
                    
            },
            error: function (xhr, ajaxOptions, thrownError) {

            }
        });

        $.ajax({
            url: "../../s.aspx/get_name",
            type: "POST",
            dataType: 'text',
            timeout:60*1000,
            contentType: "application/json; charset=utf-8",
            success: function (val) {
                Book.accountName = val;
            },
            error: function (xhr, ajaxOptions, thrownError) {

            }
        });


        /* ************************************************ */
        //跟 freak.html 要一致
        for (var i = 0; i<=12;i++){                
            var f = PIXI.utils.TextureCache["imgs/fish/freak-" + i + ".png"];
            
            this._FishItemsFreak.push({
                image: f,
                fishtype: i       
            });
        }     
        //跟 shallow.html 要一致
        for (var i = 0; i<=6;i++){                
            var f = PIXI.utils.TextureCache["imgs/fish/ks-" + i + ".png"];
            
            this._FishItemsShallow.push({
                image: f,
                fishtype: i       
            });
        }        
        //跟 deep.html 要一致
        for (var i = 0; i <= 9; i++) {
            var f = PIXI.utils.TextureCache["imgs/fish/deep-" + i + ".png"];
    
            this._FishItemsDeep.push({
                image: f,
                fishtype: i        
            });
        }  
        //跟 ryukyu.html 要一致
        for (var i = 0; i <= 7; i++) {
            var f = PIXI.utils.TextureCache["imgs/fish/ryukyu-" + i + ".png"];            

            this._FishItemsRyukyu.push({
                image: f,
                fishtype: i                
            });
        }      
        /* ************************************************ */

        
        this.group = new Container();
        this.group.width = Config.stage.w;
        this.groupPage = new Container();

        //bg
        var bookbg = new PIXI.Sprite(PIXI.utils.TextureCache["imgs/book/01_book_bg.png"]);
        bookbg.width = Config.stage.w;
        bookbg.height = bookbg.width * (1080 / 1920);

        //sea
        var sea = new PIXI.Sprite(PIXI.utils.TextureCache["imgs/book/02_book_water.png"]);
        sea.width = bookbg.width * 0.4;
        sea.height = sea.width * (870 / 762);
        sea.x = bookbg.width * 0.08;
        sea.y = bookbg.height * 0.07;
        this.spries.sea = sea;

        //seaFreak
        var seaFreak = new PIXI.Sprite(PIXI.utils.TextureCache["imgs/book/02_book_freak.png"]);
        seaFreak.width = sea.width;
        seaFreak.height = sea.height;
        seaFreak.x = sea.x;
        seaFreak.y = sea.y;
        seaFreak.visible = false;
        this.spries.seaFreak = seaFreak;        

        //seaWater
        var seaWater = new PIXI.Sprite(PIXI.utils.TextureCache["imgs/book/02_book_water.png"]);
        seaWater.width = sea.width;
        seaWater.height = sea.height;
        seaWater.x = sea.x;
        seaWater.y = sea.y;
        seaWater.visible = false;
        this.spries.seaWater = seaWater;

        //seaDeep
        var seaDeep = new PIXI.Sprite(PIXI.utils.TextureCache["imgs/book/02_book_deep.png"]);
        seaDeep.width = sea.width;
        seaDeep.height = sea.height;
        seaDeep.x = sea.x;
        seaDeep.y = sea.y;
        seaDeep.visible = false;
        this.spries.seaDeep = seaDeep;

        //seaRyukyu
        var seaRyukyu = new PIXI.Sprite(PIXI.utils.TextureCache["imgs/book/02_book_ryukyu.png"]);
        seaRyukyu.width = sea.width;
        seaRyukyu.height = sea.height;
        seaRyukyu.x = sea.x;
        seaRyukyu.y = sea.y;
        seaRyukyu.visible = false;
        this.spries.seaRyukyu = seaRyukyu;

        //LVs 有幾隻
        for (var i = 0; i <= 10; i++) {
            var sprite = new PIXI.Sprite(PIXI.utils.TextureCache["imgs/book/06_book_eng" + i + ".png"]);
            Book.LVs.push({
                id: i,
                sprite: sprite
            });
            sprite.x = sea.x + sea.width + 60;
            sprite.y = 140;
            sprite.scale.x = 0.6;
            sprite.scale.y = 0.6;
            sprite.visible = false;
        }
        //怪魚區
        var btnFreak = new PIXI.Sprite(PIXI.utils.TextureCache["imgs/book/04_book_bt00.png"]);
        btnFreak.width = bookbg.width * 0.1;
        btnFreak.height = btnFreak.width * (111 / 142);
        btnFreak.x = bookbg.width * 0.9;
        btnFreak.y = bookbg.height * 0.32;
        btnFreak.scale.x = 0.7;
        btnFreak.scale.y = 0.7;
        btnFreak.on('pointerdown', Book.clickbtnFreak);
        btnFreak.interactive = true;
        btnFreak.buttonMode = true;
        //高雄區
        var btnKS = new PIXI.Sprite(PIXI.utils.TextureCache["imgs/book/04_book_bt01.png"]);
        btnKS.width = bookbg.width * 0.1;
        btnKS.height = btnKS.width * (110 / 124);
        btnKS.x = bookbg.width * 0.9;
        btnKS.y = bookbg.height * 0.45;
        btnKS.scale.x = 0.7;
        btnKS.scale.y = 0.7;
        btnKS.on('pointerdown', Book.clickbtnKS);
        btnKS.interactive = true;
        btnKS.buttonMode = true;
        //深海區
        var btnDeep = new PIXI.Sprite(PIXI.utils.TextureCache["imgs/book/04_book_bt02.png"]);
        btnDeep.width = bookbg.width * 0.1;
        btnDeep.height = btnDeep.width * (110 / 124);
        btnDeep.x = btnKS.x;
        btnDeep.y = btnKS.y + btnDeep.height - 20;
        btnDeep.scale.x = 0.7;
        btnDeep.scale.y = 0.7;
        btnDeep.on('pointerdown', Book.clickbtnDeep);
        btnDeep.interactive = true;
        btnDeep.buttonMode = true;
        //琉球區
        var btnLC = new PIXI.Sprite(PIXI.utils.TextureCache["imgs/book/04_book_bt03.png"]);
        btnLC.width = bookbg.width * 0.1;
        btnLC.height = btnDeep.width * (110 / 124);
        btnLC.x = btnDeep.x;
        btnLC.y = btnDeep.y + btnLC.height + 10;
        btnLC.scale.x = 0.7;
        btnLC.scale.y = 0.7;
        btnLC.on('pointerdown', Book.clickbtnLC);
        btnLC.interactive = true;
        btnLC.buttonMode = true;
        //left|right
        var btnLeft = new PIXI.Sprite(PIXI.utils.TextureCache["imgs/book/05_book_bt_left.png"]);
        btnLeft.scale.x = 0.5;
        btnLeft.scale.y = 0.5;
        btnLeft.x = sea.x - btnLeft.width / 4;
        btnLeft.y = sea.y + sea.height / 2 - btnLeft.height / 2;
        btnLeft.on('pointerdown', Book.clickLeft);
        btnLeft.interactive = true;
        btnLeft.buttonMode = true;
        var btnRight = new PIXI.Sprite(PIXI.utils.TextureCache["imgs/book/05_book_bt_right.png"]);
        btnRight.scale.x = 0.5;
        btnRight.scale.y = 0.5;
        btnRight.x = sea.x + sea.width - btnRight.width / 2;
        btnRight.y = sea.y + sea.height / 2 - btnLeft.height / 2;
        btnRight.on('pointerdown', Book.clickRight);
        btnRight.interactive = true;
        btnRight.buttonMode = true;

        //close
        var close = new PIXI.Sprite(PIXI.utils.TextureCache["imgs/book/bookClose.png"]);
        close.x = bookbg.x + bookbg.width - close.width;
        close.y = 0;
        close.on('pointerdown', Book.close);
        close.interactive = true;
        close.buttonMode = true;
        //fish
        //for (var i = 0; i <= 6; i++) {
        for (var i = 0; i <= 0; i++) {
            //var fish = new PIXI.Sprite(PIXI.utils.TextureCache["imgs/book/07_book_fish" + i + ".png"]);
            var fish = new PIXI.Sprite(PIXI.utils.TextureCache["imgs/book/03_book_nofish.png"]);
            fish.width = sea.width;
            fish.height = sea.height;
            fish.visible = false;
            this.spritePages.push(fish);
            this.groupPage.addChild(fish);
            //
            Book.seaWidthKeep = sea.width;
            Book.seaHeightKeep = sea.height;
        }        
        this.groupPage.x = sea.x + sea.width + 50;
        this.groupPage.y = sea.y;

        this.spritePages[0].visible = true;
        //sale
        var sale = new PIXI.Sprite(PIXI.utils.TextureCache["imgs/book/sale.png"]);
        sale.width = 70;
        sale.height = sale.width * 115 / 215;
        sale.x = this.groupPage.x + this.spritePages[0].width - sale.width - 40;
        sale.y = this.groupPage.y + 335;
        sale.on('pointerdown', Book.showSalePanel);
        sale.interactive = true;
        sale.buttonMode = true;
        Book.button.sale = sale;
        ////////////// add
        this.group.addChild(bookbg);
        //this.group.addChild(sea);
        this.group.addChild(seaFreak);
        this.group.addChild(seaWater);
        this.group.addChild(seaDeep);
        this.group.addChild(seaRyukyu);

        this.group.addChild(btnFreak);
        this.group.addChild(btnKS);
        this.group.addChild(btnDeep);
        this.group.addChild(btnLC);
        this.group.addChild(this.groupPage);
        this.group.addChild(btnLeft);
        this.group.addChild(btnRight);
        this.group.addChild(close);
        this.group.addChild(sale);
        for (var i = 0; i <= 10; i++) {
            var sprite = this.LVs.getById(i).sprite;
            this.group.addChild(sprite);
        }
        this.group.y = 50;

        app.stage.addChild(this.group);
        this.group.visible = false;
    },
    close: function () {
        Book.group.visible = false;
    },
    hideAll: function () {
        for (var i = 0; i < Book.spritePages.length; i++) {
            Book.spritePages[i].visible = false;
        }
    },
    removeAllFishInPool: function () {        
        for (var i = 0; i < Book.pool.length; i++) {
            Book.group.removeChild(Book.pool[i]);
        }        
        Book.pool = [];
    },
    drop: function (index) {
        Book.hideAll();
        Book.removeAllFishInPool();
        Book.spritePages[index].visible = true;        

        //var fishtypesCount = Fish.type.getById(index);
        var fishtypesCount = Book._FishType.getById(index);  
        //console.log(Book._FishType);      
        var fishcount = fishtypesCount.count;        
        if (fishtypesCount == undefined) {

        } else {
            for (var i = 0; i < fishcount; i++) {
                Book.add(index);
            }
        }
        //lv
        for (var i = 0; i <= 10; i++) {
            var lv = Book.LVs[i];
            lv.sprite.visible = false;
        }
        if (fishcount >= 10) fishcount = 10;
        Book.LVs.getById(fishcount).sprite.visible = true;
    },
    clickbtnFreak: function () {        
        Book.changeRegion('fk');
    },
    clickbtnKS: function () {        
        Book.changeRegion('ks');
    },
    clickbtnDeep: function () {        
        Book.changeRegion('dp');
    },
    clickbtnLC: function () {
        Book.changeRegion('ru');
    },
    clickLeft: function () {
        Book.hideSalePanel();
        if (Book.pageIndex <= 0) return;
        Book.pageIndex--;
        Book.drop(Book.pageIndex);
    },
    clickRight: function () {
        Book.hideSalePanel();
        if (Book.pageIndex >= Book.spritePages.length - 2) return;
        Book.pageIndex++;
        Book.drop(Book.pageIndex);
    },
    hideSea: function () {
        Book.spries.seaFreak.visible = false;
        Book.spries.seaWater.visible = false;
        Book.spries.seaDeep.visible = false;
        Book.spries.seaRyukyu.visible = false;
    },
    changeRegion: function (Region) {
        //清除之前的圖片
        for (var i = 0; i < Book.spritePages.length; i++) {
            Book.groupPage.removeChild(Book.spritePages[i]);
        }       
        Book.spritePages = [];
        Book.pageIndex = 0;
        Book.hideSea();

        //顯示 怪魚區        
        if (Region == 'fk') {            
            Book.spries.seaFreak.visible = true;
            //
            Book._FishType = Book._FishTypeFreak;
            Book._FishItems = Book._FishItemsFreak;
            //
            for (var i = 0; i <= 12; i++) {
                var fish = new PIXI.Sprite(PIXI.utils.TextureCache["imgs/book/07_book_freak_fish" + i + ".png"]);
                fish.width = Book.seaWidthKeep;
                fish.height = Book.seaHeightKeep;
                fish.visible = false;
                Book.spritePages.push(fish);
                Book.groupPage.addChild(fish);            
            }
            //console.log(Book._FishItems);
        }        
        //顯示 高雄區(預設值)
        if ((Region == 'ks') || (Region == 'me')) {            
            Book.spries.seaWater.visible = true;
            //
            Book._FishType = Book._FishTypeShallow;
            Book._FishItems = Book._FishItemsShallow;
            //
            for (var i = 0; i <= 6; i++) {
                var fish = new PIXI.Sprite(PIXI.utils.TextureCache["imgs/book/07_book_fish" + i + ".png"]);
                fish.width = Book.seaWidthKeep;
                fish.height = Book.seaHeightKeep;
                fish.visible = false;
                Book.spritePages.push(fish);
                Book.groupPage.addChild(fish);            
            }
            //console.log(Book._FishItems);
        }   
        //顯示 深海區
        if (Region == 'dp') {            
            Book.spries.seaDeep.visible = true;
            //
            Book._FishType = Book._FishTypeDeep;
            Book._FishItems = Book._FishItemsDeep;
            //
            for (var i = 0; i <= 9; i++) {
                var fish = new PIXI.Sprite(PIXI.utils.TextureCache["imgs/book/07_book_deep_fish" + i + ".png"]);                
                fish.width = Book.seaWidthKeep;
                fish.height = Book.seaHeightKeep;
                fish.visible = false;
                Book.spritePages.push(fish);
                Book.groupPage.addChild(fish);            
            }
            //console.log(Book._FishItems);
        }        
        //顯示 琉球區
        if (Region == 'ru') {
            Book.spries.seaRyukyu.visible = true;
            //
            Book._FishType = Book._FishTypeRyukyu;
            Book._FishItems = Book._FishItemsRyukyu;
            //
            for (var i = 0; i <= 7; i++) {
                var fish = new PIXI.Sprite(PIXI.utils.TextureCache["imgs/book/07_book_ryukyu_fish" + i + ".png"]);                
                fish.width = Book.seaWidthKeep;
                fish.height = Book.seaHeightKeep;
                fish.visible = false;
                Book.spritePages.push(fish);
                Book.groupPage.addChild(fish);            
            }
            //console.log(Book._FishItems);
        }        
        //        
        Book.close();
        Book.open(Region);        
        //MM.alert(Region);                
    },
    add: function (index) {
        //var ff = Fish.items[index];        
        var ff = Book._FishItems[index];        
        var fs = new Sprite(ff.image);
        fs.x = randomInt(Book.spries.sea.x + 30, Book.spries.sea.x + Book.spries.sea.width - fs.width - 10);
        fs.y = randomInt(Book.spries.sea.y + 50, Book.spries.sea.y + Book.spries.sea.height - fs.height - 10);
        fs.scale.x = randomInt(3, 5) / 10;        
        fs.scale.y = fs.scale.x;        

        switch (randomInt(1, 4)) {
            case 1:
                fs.vx = 0.03;
                break;
            case 2:
                fs.vx = -0.02;
                break;
            case 3:
                fs.vx = -0.03;
                break;
            default:
                fs.vx = 0.02;
                break;
        }

        Book.group.addChild(fs);
        Book.pool.push(fs);
    },
    loop: function (delta) {

        for (var i = 0; i < Book.pool.length; i++) {
            var fs = Book.pool[i];
            fs.x += fs.vx;

            if (fs.x <= Book.spries.sea.x + 30) {
                fs.vx = -fs.vx;
            }
            if (fs.x >= Book.spries.sea.x + Book.spries.sea.width - fs.width - 10) {
                fs.vx = -fs.vx;
            }
        }
    },
    show: function () {
        Book.group.visible = true;
    },
    hide: function () {
        Book.group.visible = false;
    },
    /* sale */
    isThereFishToSale: function () {
        //var fishtypecout = Fish.type.getById(Book.pageIndex);
        var fishtypecout = Book._FishType.getById(Book.pageIndex);
        if (fishtypecout == undefined) return false;
        if (fishtypecout.count <= 0) return false;
        return true;
    },
    showSalePanel: function () {

        if (Book.isThereFishToSale() == false) {
            MM.alert('您目前沒有這種漁可以賣喔!');
            return;
        }
        //reset
        Book.saleCount = 0;
        $('#book-sale-panel-count').html('0');
        $('#book-sale-panel-money').html('0');
        //control
        var left = Book.spritePages[0].x + Book.spritePages[0].width + 200;
        var top = Book.spritePages[0].y + 100 + Book.spritePages[0].height / 2;
        document.getElementById('book-sale-panel').style.display = '';
        document.getElementById('book-sale-panel').style.left = left + 'px';
        document.getElementById('book-sale-panel').style.top = top + 'px';
    },
    hideSalePanel: function () {
        document.getElementById('book-sale-panel').style.display = 'none';
    },
    decreaseSale: function () {
        Book.saleCount--;
        if (Book.saleCount <= 0) Book.saleCount = 0;
        $('#book-sale-panel-count').html(Book.saleCount);

        //var price = Fish.type.getById(Book.pageIndex).price;
        var price = Book._FishType.getById(Book.pageIndex).price;

        $('#book-sale-panel-money').html(price * Book.saleCount);

    },
    increaseSale: function () {
        Book.saleCount++;
        //var fishtypecout = Fish.type.getById(Book.pageIndex);
        var fishtypecout = Book._FishType.getById(Book.pageIndex);
        if (fishtypecout == undefined) return;
        if (Book.saleCount >= fishtypecout.count) Book.saleCount = fishtypecout.count;
        $('#book-sale-panel-count').html(Book.saleCount);

        //var price = Fish.type.getById(Book.pageIndex).price;
        var price = Book._FishType.getById(Book.pageIndex).price;

        $('#book-sale-panel-money').html(price * Book.saleCount);
    },
    yesSale: function () {
        var count = $('#book-sale-panel-count').html();
        var fishtype = Book.pageIndex;
        var money = $('#book-sale-panel-money').html();

        count = parseInt(count);
        if (count <= 0) {
            MM.alert('至少要選一條漁才能賣喔!');
            return;
        }

        $.ajax({
            fishtype: fishtype,
            url: "data.aspx/sale_fish",
            data: "{ money:\"" + money + "\",count:\"" + count + "\",type:\"" + fishtype + "\"}",
            type: "POST",
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            success: function (okcount) {

                if (okcount == '0') {
                    MM.alert('賣出失敗 商店有問題');
                    return;
                }
                okcount = parseInt(okcount);
                MM.alert('成功賣出');
                Wallet.query();
                //var fishtype = Fish.type.getById(this.fishtype);
                var fishtype = Book._FishType.getById(this.fishtype);
                fishtype.count -= count;

                Book.drop(this.fishtype);
                Book.hideSalePanel();
            },
            error: function (xhr, ajaxOptions, thrownError) {

            }
        });
    },
    noSale: function () {
        Book.hideSalePanel();
    },
    addFish: function (page, fishtype) {
        if (Book.isOnline == false) {
            MM.alert('完整版遊戲，請<a href="../../login.aspx">登入</a>或<a href="../../login.aspx">註冊</a>。');
            return;
        }   
             
        $.ajax({
            url: "data.aspx/add_fish",
            data: "{ page:\""+page+"\",fishtype:\"" + fishtype + "\"}",
            type: "POST",
            dataType: 'xml',
            contentType: "application/json; charset=utf-8",
            success: function (xml) {
                
            },
            error: function (xhr, ajaxOptions, thrownError) {

            }
        });
    },
    open: function (page) {
        if (Book.isOnline == false) {
            MM.alert('完整版遊戲，請<a href="../../login.aspx">登入</a>或<a href="../../login.aspx">註冊</a>。');
            return;
        }
        MM.mask.show();
        $.ajax({
            url: "data.aspx/get_fishtype",
            data: "{ page:\""+page+"\"}",
            type: "POST",
            dataType: 'xml',
            contentType: "application/json; charset=utf-8",
            success: function (xml) {
                MM.mask.hide();
                Book.fishtypes = [];
                $(xml).find("Table").each(function () {

                    var CNT = $(this).find("CNT").eq(0).text();
                    var FISHTYPE = $(this).find("FISHTYPE").eq(0).text();

                    FISHTYPE = parseInt(FISHTYPE);

                    //var ft = Fish.type.getById(FISHTYPE);
                    var ft = Book._FishType.getById(FISHTYPE);
                    if (ft != undefined) {
                        ft.count = parseInt(CNT);
                    }

                });
                
                Book.show();
                Book.drop(0);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                MM.mask.hide();
            }
        });
    }
}
var Shop = {
    group: null,
    groupItem: null,
    items: [],
    shopitembg: null,
    salecounttext: null,
    costtext: null,
    cartcount: 0,
    init: function () {
        //
        this.group = new Container();
        this.groupItem = new Container();
        var shop = new PIXI.Sprite(PIXI.utils.TextureCache["imgs/04_game1_shop.png"]);
        shop.width = Config.stage.w * 0.65;
        shop.height = shop.width * (940 / 1260);
        shop.x = Config.stage.w * 0.18;
        shop.y = Config.stage.w * 0.1;

        //close 
        var close = new PIXI.Sprite(PIXI.utils.TextureCache["imgs/book/bookClose.png"]);        
        close.x = shop.x + shop.width - close.width - 30;
        close.y = shop.y + 20;
        close.on('pointerdown', Shop.close);
        close.interactive = true;
        close.buttonMode = true;

        //battery button
        var batteryButton = new Graphics();
        batteryButton.alpha = 0;
        batteryButton.cursor = 'pointer';
        batteryButton.beginFill(0x66FF33);
        batteryButton.drawPolygon([
            0, 130,             //First point
            140, 130,              //Second point
            140, 0,
            0, 0                 //Third point
        ]);

        batteryButton.endFill();
        batteryButton.x = (shop.x + 200) * __mapScale;
        batteryButton.y = (shop.y + 120) * __mapScale;
        batteryButton.t = 'battery';
        batteryButton.interactive = true
        batteryButton.click = Shop.goCart;

        //shop item
        var shopitembg = new PIXI.Sprite(PIXI.utils.TextureCache["imgs/04_game1_shop2.png"]);
        shopitembg.width = Config.stage.w * 0.65;
        shopitembg.height = shop.width * (940 / 1260);
        shopitembg.x = Config.stage.w * 0.18;
        shopitembg.y = Config.stage.w * 0.1;
        Shop.shopitembg = shopitembg;
        this.groupItem.addChild(Shop.shopitembg);
        //battery item
        var batteryitem = new PIXI.Sprite(PIXI.utils.TextureCache["imgs/04_game1_shop_object1.png"]);
        batteryitem.width = shopitembg.width / 2.2;
        batteryitem.height = batteryitem.width * (449 / 626);
        batteryitem.x = (shopitembg.x + (shopitembg.width / 2)) - (batteryitem.width / 2);
        batteryitem.y = shopitembg.y + 20;
        Shop.items.push({
            id: 'battery',
            price: 3,
            sprite: batteryitem
        });
        this.groupItem.addChild(batteryitem);
        //add item button
        var additem = new Graphics();
        additem.alpha = 0;
        additem.cursor = 'pointer';
        additem.beginFill(0x66FF33);
        additem.drawPolygon([
            0, 40,             //First point
            40, 40,              //Second point
            40, 0,
            0, 0                 //Third point
        ]);

        additem.endFill();

        additem.x = (shopitembg.x + (shopitembg.width / 2)) + 40;
        additem.y = (shopitembg.y + (shopitembg.height / 2)) + 55;
        additem.t = 'battery';
        additem.interactive = true
        additem.click = Shop.additem;
        this.groupItem.addChild(additem);
        //salecounttext
        let style = new TextStyle({
            fontFamily: "Arial",
            fontSize: 40,
            fill: "red"
        });
        Shop.salecounttext = new Text("0", style);
        var x = (shopitembg.x + (shopitembg.width / 2)) - 50;
        var y = (shopitembg.y + (shopitembg.height / 2)) + 50;
        Shop.salecounttext.position.set(x, y);
        this.groupItem.addChild(Shop.salecounttext);
        //costtext
        style = new TextStyle({
            fontFamily: "Arial",
            fontSize: 34,
            fill: "black"
        });
        Shop.costtext = new Text("0$", style);
        var x = (shopitembg.x + (shopitembg.width / 2)) - 80;
        var y = (shopitembg.y + (shopitembg.height / 2)) + 110;
        Shop.costtext.position.set(x, y);
        this.groupItem.addChild(Shop.costtext);

        //yes buy button
        var yesbuy = new Graphics();
        yesbuy.alpha = 0;
        yesbuy.cursor = 'pointer';
        yesbuy.beginFill(0x66FF33);
        yesbuy.drawPolygon([
            0, 40,             //First point
            100, 40,              //Second point
            100, 0,
            0, 0                 //Third point
        ]);

        yesbuy.endFill();

        yesbuy.x = (shopitembg.x + (shopitembg.width / 2)) + -10;
        yesbuy.y = (shopitembg.y + (shopitembg.height / 2)) * __mapScale + 205;
        yesbuy.interactive = true
        yesbuy.click = Shop.pay;
        this.groupItem.addChild(yesbuy);

        //no buy button
        var nobuy = new Graphics();
        nobuy.alpha = 0;
        nobuy.cursor = 'pointer';
        nobuy.beginFill(0x66FF33);
        nobuy.drawPolygon([
            0, 40,             //First point
            100, 40,              //Second point
            100, 0,
            0, 0                 //Third point
        ]);

        nobuy.endFill();

        nobuy.x = (shopitembg.x + (shopitembg.width / 2)) + 100;
        nobuy.y = (shopitembg.y + (shopitembg.height / 2)) * __mapScale + 205;
        nobuy.interactive = true
        nobuy.click = Shop.nobuy;
        this.groupItem.addChild(nobuy);
        /////
        this.groupItem.visible = false;
        //group
        this.group.x = 0;
        this.group.y = 0;
        this.group.addChild(shop);
        this.group.addChild(close);
        this.group.addChild(batteryButton);
        this.group.addChild(this.groupItem);
        this.group.visible = false;
        app.stage.addChild(this.group);
    },
    close: function () {
        Shop.hide();
    },
    open: function () {
        if (Book.isOnline == false) {
            MM.alert('完整版遊戲，請<a href="../../login.aspx">登入</a>或<a href="../../login.aspx">註冊</a>。');
            return;
        }
        Shop.show();
    },
    show: function () { Shop.group.visible = true; },
    hide: function () { Shop.group.visible = false; },
    goCart: function () {
        //購買電池
        recordBattery();
        getBattery();
        MM.alert('電池買到了');
        Shop.close();
        
        /*
        Shop.groupItem.visible = true;
        Shop.items.getById('battery').sprite.visible = true;
        */
    },
    additem: function () {
        Shop.cartcount++;
        var price = Shop.items.getById('battery').price;
        Shop.salecounttext.text = Shop.cartcount;
        Shop.costtext.text = '-' + (price * Shop.cartcount) + '$';
    },
    pay: function () {
        Shop.groupItem.visible = false;
        MM.alert('買到了');
        var cost = parseInt(Shop.costtext.text)
        Wallet.withdraw('battery', cost);
        Shop.resetCart();
    },
    nobuy: function () {
        Shop.groupItem.visible = false;
        Shop.resetCart();
    },
    resetCart: function () {
        Shop.salecounttext.text = 0;
        Shop.costtext.text = 0 + '$';
        Shop.cartcount = 0;
    }
}
var Menu = {
    sprites: {
        menu: null, shop: null, map: null, book: null
    },
    init: function () {
        this.sprites.menu = new PIXI.Sprite(PIXI.utils.TextureCache["imgs/04_game1_btn1.png"]);
        this.sprites.menu.x = 20;        
        this.sprites.menu.y = __menuY;
        this.sprites.menu.scale.x = __menuScaleX;
        this.sprites.menu.scale.y = __menuScaleY;
        this.sprites.menu.on('pointerdown', Menu.clickMenu);
        this.sprites.menu.interactive = true;
        this.sprites.menu.buttonMode = true;
        this.sprites.shop = new PIXI.Sprite(PIXI.utils.TextureCache["imgs/04_game1_btn2.png"]);
        this.sprites.shop.x = 20;        
        this.sprites.shop.y = __shopY;
        this.sprites.shop.scale.x = __shopScaleX;
        this.sprites.shop.scale.y = __shopScaleY;
        this.sprites.shop.on('pointerdown', Menu.clickShop);
        this.sprites.shop.interactive = true;
        this.sprites.shop.buttonMode = true;
        this.sprites.map = new PIXI.Sprite(PIXI.utils.TextureCache["imgs/04_game1_btn3.png"]);
        this.sprites.map.x = 20;        
        this.sprites.map.y = __mapY;
        this.sprites.map.scale.x = __mapScaleX;
        this.sprites.map.scale.y = __mapScaleY;
        this.sprites.map.on('pointerdown', Menu.clickMap);
        this.sprites.map.interactive = true;
        this.sprites.map.buttonMode = true;
        this.sprites.book = new PIXI.Sprite(PIXI.utils.TextureCache["imgs/04_game1_btn4.png"]);
        this.sprites.book.x = 20;        
        this.sprites.book.y = __bookY;
        this.sprites.book.scale.x = __bookScaleX;
        this.sprites.book.scale.y = __bookScaleY;
        this.sprites.book.on('pointerdown', Menu.clickBook);
        this.sprites.book.interactive = true;
        this.sprites.book.buttonMode = true;
        app.stage.addChild(this.sprites.menu);
        app.stage.addChild(this.sprites.shop);
        app.stage.addChild(this.sprites.map);
        app.stage.addChild(this.sprites.book);
    },
    clickMenu: function () {
        MenuShip.close();
        Shop.close();
        Map.close();
        Book.close();
        //
        var name = '';
        switch (__CodeName) {
            case 'ks':
                name = shallowName;
                break;
            case 'dp':
                name = deepName;
                break;
            case 'me':
                name = mazeName;
                break;
            case 'fk':
                name = freakName;
                break;
            case 'ru':
                name = ryukyuName;
                break;        
        }        
        MenuShip.open(name);
    },
    clickShop: function () { 
        MenuShip.close();
        Shop.close();
        Map.close();
        Book.close();
        //
        Shop.open();
    },
    clickMap: function () {
        MenuShip.close();
        Shop.close();
        Map.close();
        Book.close();
        //
        Map.open();
    },
    clickBook: function () {  
        MenuShip.close();
        Shop.close();
        Map.close();
        Book.close();
        //        
        //Book.open(__PAGE);
        Book.changeRegion(__PAGE);      
        //MM.alert(__PAGE);  
    }
}
var MenuShip = {
    accountNametext: null,
    nametext: null,
    scoretext: null,
    group: null,
    init: function () {

        this.group = new Container();

        var ship = new PIXI.Sprite(PIXI.utils.TextureCache["imgs/04_game1_menu.png"]);
        ship.width = Config.stage.w * 0.65;
        ship.height = ship.width * (878 / 1338);
        ship.x = Config.stage.w * 0.18;
        ship.y = Config.stage.w * 0.1;

        //close
        var close = new PIXI.Sprite(PIXI.utils.TextureCache["imgs/book/bookClose.png"]);
        close.x = ship.x + ship.width - close.width;
        close.y = ship.y;
        close.on('pointerdown', MenuShip.close);
        close.interactive = true;
        close.buttonMode = true;
        
        //accountName
        let style = new TextStyle({
            fontFamily: "Arial",
            fontSize: 28,
            fill: "black"
        });
        MenuShip.accountNametext = new Text('accountName', style);        
        var x = ((ship.x + (ship.width / 2)) - 160) + (__mapWidth / 10);
        var y = ((ship.y + (ship.height / 2)) + 130) - ((__mapWidth / 10) * 2);
        MenuShip.accountNametext.position.set(x, y);        

        //name
        style = new TextStyle({
            fontFamily: "Arial",
            fontSize: 28,
            fill: "black"
        });
        MenuShip.nametext = new Text("淺水區", style);
        var x = (ship.x + (ship.width / 2)) + 60;
        var y = (ship.y + (ship.height / 2)) + 0;
        MenuShip.nametext.position.set(x, y);        

        //score
        style = new TextStyle({
            fontFamily: "Arial",
            fontSize: 28,
            fill: "black"
        });
        MenuShip.scoretext = new Text("0", style);
        var x = MenuShip.nametext.x + 0;
        var y = (MenuShip.nametext.y + 75) - (__mapWidth / 10);
        MenuShip.scoretext.position.set(x, y);

        //group
        this.group.x = 0;
        this.group.y = 0;
        this.group.addChild(ship);
        this.group.addChild(close);
        this.group.addChild(MenuShip.accountNametext);        
        this.group.addChild(MenuShip.nametext);
        this.group.addChild(MenuShip.scoretext);
        this.group.visible = false;
        app.stage.addChild(this.group);
    },
    close: function () {
        MenuShip.hide();
    },
    open: function (val) {
        if (Book.isOnline == false) {
            MM.alert('完整版遊戲，請<a href="../../login.aspx">登入</a>或<a href="../../login.aspx">註冊</a>。');
            return;
        }
        //  
        MenuShip.accountNametext.text = Book.accountName;      
        MenuShip.nametext.text = val;
        $.ajax({
            url: "data.aspx/query_wallet",
            type: "POST",
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            success: function (money) {
                MenuShip.scoretext.text = money;                            
            },
            error: function (xhr, ajaxOptions, thrownError) {

            }
        });        
        MenuShip.show();
    },
    show: function () { MenuShip.group.visible = true; },
    hide: function () { MenuShip.group.visible = false; }
}
var Map = {
    group: null,
    isTargetMaze:false,
    isTargetFreak:false,    
    init: function () {
        //maze
        $.ajax({
            url: "../../s.aspx/get_record",
            type: "POST",
            data: "{\"id\":\"maze\"}",
            dataType: 'text',
            contentType: "application/json; charset=utf-8",
            success: function (target) {
                if (target == '0') {
                    
                } else {                    
                    Map.isTargetMaze = true;
                }
                    
            },
            error: function (xhr, ajaxOptions, thrownError) {

            }
        });
        //freak
        $.ajax({
            url: "../../s.aspx/get_record",
            type: "POST",
            data: "{\"id\":\"freak\"}",
            dataType: 'text',
            contentType: "application/json; charset=utf-8",
            success: function (target) {
                if (target == '0') {

                } else {                    
                    Map.isTargetFreak = true;
                }
                    
            },
            error: function (xhr, ajaxOptions, thrownError) {

            }
        });


        this.group = new Container();

        //# bg
        var bg = new PIXI.Sprite(PIXI.utils.TextureCache["imgs/05_menu_map.png"]);
        bg.width = 800 - __mapWidth;
        bg.height = bg.width * (800 / 970);
        bg.x = Config.stage.w / 2 - (bg.width / 2);
        bg.y = 20;

        //# close button
        var closeButton = new Graphics();
        closeButton.alpha = 0;
        closeButton.cursor = 'pointer';
        closeButton.beginFill(0x66FF33);
        closeButton.drawPolygon([
            0, 70,             //First point
            100, 70,              //Second point
            100, 0,
            0, 0                 //Third point
        ]);

        closeButton.endFill();
        closeButton.x = bg.x + bg.width - 120;
        closeButton.y = bg.y;
        closeButton.interactive = true;
        closeButton.click = Map.close;

        //# 淺水區
        var shallow = new Graphics();
        shallow.alpha = 0;
        shallow.cursor = 'pointer';
        shallow.beginFill(0x66FF33);
        shallow.drawPolygon([
            0, 70,             //First point
            100, 70,              //Second point
            100, 0,
            0, 0                 //Third point
        ]);

        shallow.endFill();
        shallow.x = (bg.x + 210) * __mapScale;
        shallow.y = (bg.y + 60) * __mapScale;
        shallow.interactive = true;
        shallow.click = function () {
            $("canvas").fadeOut(600, function () {
                location.href = 'shallow.html';
            });
        };

        //# 深海區
        var deep = new Graphics();
        deep.alpha = 0;
        deep.cursor = 'pointer';
        deep.beginFill(0x66FF33);
        deep.drawPolygon([
            0, 70,             //First point
            100, 70,              //Second point
            100, 0,
            0, 0                 //Third point
        ]);

        deep.endFill();
        deep.x = (bg.x +160) * __mapScale;
        deep.y = (bg.y +160) * __mapScale;
        deep.interactive = true;
        deep.click = function () {
            $("canvas").fadeOut(600, function () {
                location.href = 'deep.html';
            });
        };

        //# 金幣區
        var maze = new Graphics();
        maze.alpha = 0;
        maze.cursor = 'pointer';
        maze.beginFill(0x66FF33);
        maze.drawPolygon([
            0, 70,             //First point
            100, 70,              //Second point
            100, 0,
            0, 0                 //Third point
        ]);

        maze.endFill();
        maze.x = (bg.x + 260) * __mapScale;
        maze.y = (bg.y + 360) * __mapScale;
        maze.interactive = true;
        maze.click = function () {
            if (Map.isTargetMaze == false) {
                MM.alert('尚未玩過[' + mazeName + ']，無法進入');
                return;
            }
            //
            $("canvas").fadeOut(600, function () {
                location.href = 'maze.html';
            });
        };

        //# 隱藏區
        var freak = new Graphics();
        freak.alpha = 0;
        freak.cursor = 'pointer';
        freak.beginFill(0x66FF33);
        freak.drawPolygon([
            0, 70,             //First point
            100, 70,              //Second point
            100, 0,
            0, 0                 //Third point
        ]);

        freak.endFill();
        freak.x = (bg.x + 350) * __mapScale;
        freak.y = (bg.y + 450) * __mapScale;
        freak.interactive = true;
        freak.click = function () {
            if (Map.isTargetFreak == false) {
                MM.alert('尚未玩過[' + freakName + ']，無法進入');
                return;
            }
            //
            $("canvas").fadeOut(600, function () {
                location.href = 'freak.html';
            });
        };

        //# 琉球區
        var ryukyu = new Graphics();
        ryukyu.alpha = 0;
        ryukyu.cursor = 'pointer';
        ryukyu.beginFill(0x66FF33);
        ryukyu.drawPolygon([
            0, 70,             //First point
            100, 70,              //Second point
            100, 0,
            0, 0                 //Third point
        ]);

        ryukyu.endFill();
        ryukyu.x = (bg.x + 440) * __mapScale;
        ryukyu.y = (bg.y + 540) * __mapScale;
        ryukyu.interactive = true;
        ryukyu.click = function () {
            $("canvas").fadeOut(600, function () {
                location.href = 'ryukyu.html';
            });
        };

        //# group
        this.group.visible = false;
        this.group.addChild(bg);
        this.group.addChild(closeButton);
        this.group.addChild(shallow);
        this.group.addChild(deep);
        this.group.addChild(maze);
        this.group.addChild(freak);        
        this.group.addChild(ryukyu);
        app.stage.addChild(this.group);

    },
    close: function () {
        Map.hide();
    },
    open: function () {
        if (Book.isOnline == false) {
            MM.alert('完整版遊戲，請<a href="../../login.aspx">登入</a>或<a href="../../login.aspx">註冊</a>。');
            return;
        }
        Map.show();
    },
    show: function () { Map.group.visible = true; },
    hide: function () { Map.group.visible = false; }
}
