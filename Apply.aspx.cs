﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Apply : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btn_apply_Click(object sender, EventArgs e)
    {
        string name = txt_name.Text;
        string email = txt_email.Text;
        string password = txt_password.Text;
        string lon = hid_lon.Value;
        string lat = hid_lat.Value;

        if (name == "" || email == "" || password == "")
        {
            MM.JSClass.run("MM.alert('請完整輸入資訊');");
            return;
        }

        string sql = "";
        Common.MSSQL_M db = new Common.MSSQL_M();
        ArrayList par = new ArrayList();

        sql = "SELECT COUNT(*) C FROM ACCOUNT WHERE EMAIL=@EMAIL";
        par.Add(new SqlParameter("@EMAIL", email));
        int r = db.ExecuteScalar(sql,par);
        if (r >= 1)
        {
            MM.JSClass.run("MM.alert('email重複申請');");
            return;
        }
        par.Clear();
        sql = @"
            INSERT INTO [dbo].[ACCOUNT]
                       ([NAME]
                       ,[EMAIL]
                       ,[PASSWORD])
                 VALUES
                       (@NAME
                       ,@EMAIL
                       ,@PASSWORD)
        ";
        par.Add(new SqlParameter("@NAME", name));
        par.Add(new SqlParameter("@EMAIL", email));
        par.Add(new SqlParameter("@PASSWORD", password));
        r = db.Execute(sql, par);

        MM.JSClass.run(@"
            MM.alert('申請成功,自動轉至登入頁面');
            setTimeout(function(){
                location.href = 'login.aspx';
            },1000);
        ");
    }
}